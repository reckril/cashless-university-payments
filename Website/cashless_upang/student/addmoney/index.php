<?php
    include "../db_con.php";
    $link_settings = "../settings/?id=".$_GET['id'];
    $link_trans = "../transactions/?id=".$_GET['id'];
    $link_paynow = "../paynow/?id=".$_GET['id'];
    $link_home = "../?id=".$_GET['id'];
    $sql = "SELECT * FROM tbl_students WHERE id = ".$_GET['id'];
    $result = mysqli_query($conn, $sql); 
    if(mysqli_num_rows($result) === 1){
        $row = mysqli_fetch_assoc($result);
            $admin_lname = $row['lname'];
            $admin_fname = $row['fname'];
            $admin_mi = $row['mi'];
            $admin_studnum = $row['studnum'];
            $balance = $row['balance'];
            // $pay1 = $row['pay1'];
            // $pay2 = $row['pay2'];
            // $pay3 = $row['pay3'];
    }
    if(empty($balance)){
        $balance = "0.00";
    }
    // if(empty($pay1)){
    //     $pay1 = "0.00";
    // }
    // if(empty($pay2)){
    //     $pay2 = "0.00";
    // }
    // if(empty($pay3)){
    //     $pay3 = "0.00";
    // }
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/solid.min.css" integrity="sha512-WTx8wN/JUnDrE4DodmdFBCsy3fTGbjs8aYW9VDbW8Irldl56eMj8qUvI3qoZZs9o3o8qFxfGcyQocUY0LYZqzQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/fontawesome.min.css" integrity="sha512-Rcr1oG0XvqZI1yv1HIg9LgZVDEhf2AHjv+9AuD1JXWGLzlkoKDVvE925qySLcEywpMAYA/rkg296MkvqBF07Yw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="sidebar">
        <div class="logo-details">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span class="logo_name">CUP UPANG</span>
        
        </div>
        <i class='bx bx-menu' id="btn"></i>
        <ul class="nav-links">
            <li>
                <a href="<?php echo $link_home;?>">
                    <i class='bx bxs-dashboard'></i>
                    <span class="link_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="">
                    <i class="fa-solid fa-money-bill"></i>
                    <span class="link_name">Add Money</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_paynow;?>">
                    <i class="fa-solid fa-coins"></i>
                    <span class="link_name">Pay Fees</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_trans;?>">
                        <i class="fa-solid fa-receipt"></i>
                    <span class="link_name">Transactions</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_settings;?>">
                    <i class='bx bx-cog'></i>
                    <span class="link_name">Settings</span>
                </a>
                <span class="tooltip">Settings</span>
            </li>
        </ul>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <i class='bx bxs-user-circle'></i>
                    <div class="name_job">
                        <div class="name"><?php echo $row['studnum'];?></div>
                        <div class="job">Student</div>
                </div>
                </div>
                <i class='bx bx-log-out' id=log_out></i>
            </div>
            
        </div>
        
    </div>
    <script>
        let btn = document.querySelector("#btn");
        let sidebar = document.querySelector(".sidebar");
        let logout = document.querySelector("#log_out");
        btn.onclick = function(){
            sidebar.classList.toggle("active");
        }
        logout.onclick = function(){
            var r = confirm("Are you sure you want to logout???");
            if(r==true){
                window.location.href = "../../";
            }
        }
    </script>

</div>
    
    <div class="home-section">
        <div class="cardBox">
            <div class="card">
                <div>
                    <div class ="cardName">Your Account Balance is:</div>
                    <div class="numbers"><i class="fa-solid fa-peso-sign"></i><?php echo $balance;?></div>
                    
                </div>
                <!-- <div class="iconBox"><i class='bx bxs-receipt bx-rotate-180'></i></div> -->
            </div>
        </div>



        <!-- #################################################################################### -->
        <!-- Paymaya Modal -->
        
        <div class="modal fade" id="paymaya_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Money Via Paymaya</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="addPaymaya.php" method="POST">
                    <div class="modal-body">
                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                    <div class="mb-3">
                        <label for="formGroupExampleInput" class="form-label">Enter Phone Number</label>
                        <input type="text" id="phoneNum" class="form-control" id="formGroupExampleInput" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="example: 09123456789" name='phoneNum' required="required">
                    </div>
                    <label for="input-group-text" class="form-label">Amount</label>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-peso-sign"></i></span>
                            <input type="text" id="money" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Minimum of 50 pesos" name='amount' onkeypress='return event.charCode >= 48 && event.charCode <= 57' required="required">
                        <span class="input-group-text">.00</span>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" name="insert">Add Money</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            // document.getElementById("money").onchange = function() {setTotal()}
            // function setTotal(){
            //     let x = parseFloat("<?php echo $balance;?>");
            //     let y = parseFloat(document.getElementById("money").value);
            //     let z = x + y;
            //     document.getElementById("total").value = z;

            // }
        </script>
            
        <!-- #################################################################################### -->
        <!-- G-Cash Modal -->

        <div class="modal fade" id="gcash_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Money Via G-Cash</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="addGcash.php" method="post">
                    <div class="modal-body">
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                        <input type="text" name="total" id="total2" readonly=readonly required="required" hidden>
                    <div class="mb-3">
                        <label for="formGroupExampleInput" class="form-label">Enter Phone Number</label>
                        <input type="text" id="phoneNum" class="form-control" id="formGroupExampleInput" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="example: 09123456789" name='phoneNum' required="required">
                    </div>
                    <label for="input-group-text" class="form-label">Amount</label>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-peso-sign"></i></span>
                            <input type="text" id="money2" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Minimum of 50 pesos" name='amount' onkeypress='return event.charCode >= 48 && event.charCode <= 57' required="required">
                        <span class="input-group-text">.00</span>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" name="insert">Save changes</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            document.getElementById("money2").onchange = function() {setTotal1()}
            function setTotal1(){
                let x = parseFloat("<?php echo $balance;?>");
                let y = parseFloat(document.getElementById("money2").value);
                let z = x + y;
                document.getElementById("total2").value = z;

            }
        </script>

        <!-- #################################################################################### -->
        <!-- 7-eleven Modal -->

        <div class="modal fade" id="seven_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Money via 7-Eleven</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Unfortunately 7-Eleven Services does not support our fill-ups <br> please go through on our instructions!!<br><br>
                        ####################################<br>
                        <h3>Instructions</h3>
                        ####################################<br>
                        <div class="font-change">
                        1. Go to Any Cliqq Kiosk <br>
                        2. Find Bills Payment <br>
                        3. Search for Biller "Cashless University Payments PHINMA UPANG" <br>
                        4. Enter the Details <br>
                        5. Once Done Click Confirm, A barcode receipt will be printed out<br>
                        6. Present the Receipt to the Counter and Pay the exact amount given <br><br>
                        Take Note: There will be additional payments for Convinience Fee.
                        </div>
                        


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- #################################################################################### -->
        <!-- BPI Modal -->

        <div class="modal fade" id="bpi_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Money via BPI</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        BPI Services are not yet Available... <br/> Please go through on our instructions!! <br> <br>
                        ####################################<br>
                        <h3>Instructions</h3>
                        ####################################<br>
                        <div class="font-change">
                        1. Go through the BPI Online App can be downloaded on Google Play or Apple App Store and Login <br> Take Note: You have to be registered in their Physical Customer Service to use their services<br>
                        2. Open the Menu Screen located at the Upper Left Part of the screen
                        3. If you haven't added us as a Biller please go trough steps 3.a. to 3.c., If you did skip this step <br>
                        3.a. Go to "Other Services", Under "Manage Receipients", Select Manage Billers and Payees. <br>
                        3.b. Tap Add New Recipient, Under Recipient Select Recipient as Billers, Under Billers Select "Cashless University Payments PHINMA Upang" and add any Reference Number, and Press Next and Confirm. <br>
                        3.c. Once Done you may now continue step 4. <br>
                        4. Go to "Payments/Load", Under "Pay Bills", Fill out the form, then click "Next" and "Confirm".
                        <br>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- #################################################################################### -->


        <?php 
                if(isset($_GET['msg'])){ 
                    if($_GET['msg'] === "paymaya"){
                        ?><div class="success"><p>Transaction Complete!<br>Your Payment will be deducted to your Paymaya Account.</p></div><?php
                    }else if($_GET['msg'] === "gcash"){
                        ?><div class="success"><p>Transaction Complete!<br>Your Payment will be deducted to your G-Cash Account.</p></div><?php
                    }else if($_GET['msg'] === "lowP"){
                        ?><div class="error"><p>For Depositing in Paymaya Please Add the Minimum of PHP 50.00 before proceeding</p></div><?php
                    }else if($_GET['msg'] === "lowG"){
                        ?><div class="success"><p>For Depositing in G-Cash Please Add the Minimum of PHP 50.00 before proceeding</p></div><?php
                    }else if($_GET['msg'] === "delete"){
                        ?><div class="success"><p>We have updated everything successfully!</p></div><?php
                    }else if($_GET['msg'] === "nochange"){
                        ?><div class="error"><p>Update Failed: No Changes</p></div><?php
                    }else if($_GET['msg'] === "dupli"){
                        ?><div class="error"><p>Duplication Error! Please Delete the other Duplicated Field</p></div><?php
                    }else if($_GET['msg'] === "error"){
                        ?><p class="error">System has Failed to do action!</p><?php
                    }else if($_GET['msg'] === "delerror"){
                        ?><p class="error">System has Failed to delete this record!</p><?php
                    }else{
                        ?><div class="success"><p><?php echo $_GET['msg']?></p></div><?php
                    }
                }?>

        <div class="details">
            <div class="appDex">
                <div class="appHeader">
                <h2>Add Money via:</h2>
                </div>
                <div class="appBox">
                <button class="app" data-bs-toggle="modal" data-bs-target="#paymaya_modal">
                    <div class="appIcon"><img src="img/paymaya.png" alt="paymaya logo"></div>
                    <div class="appName">Paymaya</div>
                </button>
                <button class="app" data-bs-toggle="modal" data-bs-target="#gcash_modal">
                    <div class="appIcon"><img src="img/gcash.png" alt="gcash Logo"></div>
                    <div class="appName">G-Cash</div>
                </button>
                <button class="app" data-bs-toggle="modal" data-bs-target="#seven_modal">
                    <div class="appIcon"><img src="img/seveneleven.png" alt="7-Eleven Logo"></div>
                    <div class="appName">7-Eleven</div>
                </button>
                <button class="app" data-bs-toggle="modal" data-bs-target="#bpi_modal">
                    <div class="appIcon"><img src="img/bpi.jpg" alt="BPI Logo"></div>
                    <div class="appName" style="font-size: 12px; font-weight:500;">Bank of the Philippine Islands</div>
                </button>
            </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>

</html>
<!-- <div class="home-content">
        <div class="overview-boxes">
            <div class="box">
                <div class="left-side">
                    <div class="box_topic">Tenants</div>
                    <div class="number"></div>
                    <div class="indicator">
                        <i class='bx bx-basket'></i>
                    </div>
                </div>
            </div>
        </div>
        </div> -->