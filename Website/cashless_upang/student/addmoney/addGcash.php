<?php
    include '../db_con.php';
    if(isset($_POST["insert"])){
        $ad_id = $_POST['id'];
        $phone = $_POST['phoneNum'];
        $amount = str_replace(',','',$_POST['amount']);
        $num_amount = (($amount + 0.01) - 0.01);
        $count_sql = "SELECT * FROM tbl_transactions";
        $count_result = mysqli_query($conn,$count_sql);
        $count = mysqli_num_rows($count_result) + 1;
        if($num_amount >= 50.00){
            $sql="SELECT * FROM tbl_students WHERE id=".$ad_id;
            $result = mysqli_query($conn,$sql);
            if(mysqli_num_rows($result) === 1){
                $row=mysqli_fetch_assoc($result);
                $rfid = $row['rfidnum'];
                $studnum = $row['studnum'];
                $balance = $row['balance'];
                if($balance === null){
                    $balance = "0.00";
                }
                $string_balance = str_replace(',','',$balance);
                $num_balance = (($string_balance + 0.01) - 0.01);
                $total = $num_balance + $num_amount;
                $string_total = number_format($total,2,'.','');
                $final_amount = number_format($amount,2,'.','');
                $sql="UPDATE tbl_students SET balance = '".$string_total."' WHERE id=".$ad_id;
                $result=mysqli_query($conn,$sql);
                if($result){
                    $sql="INSERT INTO `tbl_transactions` (`id`, `transnumber`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `direct`, `date`, `time`, `amount`, `cart`) VALUES (NULL, ".$count.", 'Deposit via G-Cash via Online', '".$studnum."', '".$rfid."', 'STUDENT', 'ONLINE', 'GCASH', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."', NULL)";
                    $result=mysqli_query($conn,$sql);
                    header("Location: index.php?id=".$ad_id."&msg=gcash");
                    exit();
                    }
                }else{
                    header("Location: index.php?id=".$ad_id."&msg=error");
                    exit();
                }
        }else{
            header("Location: index.php?id=".$ad_id."&msg=lowG");
            exit();
            }
    }else{
        header("Location: index.php?id=".$ad_id."&msg=error");
        exit();
    }
?>
