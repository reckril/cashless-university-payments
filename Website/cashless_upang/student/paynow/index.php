<?php
    include "../db_con.php";
    $link_settings = "../settings/?id=".$_GET['id'];
    $link_trans = "../transactions/?id=".$_GET['id'];
    $link_home = "../?id=".$_GET['id'];
    $link_add = "../addmoney/?id=".$_GET['id'];
    $sql = "SELECT * FROM tbl_students WHERE id = ".$_GET['id'];
    $result = mysqli_query($conn, $sql); 
    if(mysqli_num_rows($result) === 1){
        $row = mysqli_fetch_assoc($result);
            $admin_lname = $row['lname'];
            $admin_fname = $row['fname'];
            $admin_mi = $row['mi'];
            $admin_studnum = $row['studnum'];
            $balance = $row['balance'];
            $pay1 = $row['pay1'];
            $pay2 = $row['pay2'];
            $pay3 = $row['pay3'];
    }
    if(empty($balance)){
        $balance = "0.00";
    }
    if(empty($pay1)){
        $pay1 = "0.00";
    }
    if(empty($pay2)){
        $pay2 = "0.00";
    }
    if(empty($pay3)){
        $pay3 = "0.00";
    }
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/solid.min.css" integrity="sha512-WTx8wN/JUnDrE4DodmdFBCsy3fTGbjs8aYW9VDbW8Irldl56eMj8qUvI3qoZZs9o3o8qFxfGcyQocUY0LYZqzQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/fontawesome.min.css" integrity="sha512-Rcr1oG0XvqZI1yv1HIg9LgZVDEhf2AHjv+9AuD1JXWGLzlkoKDVvE925qySLcEywpMAYA/rkg296MkvqBF07Yw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="sidebar">
        <div class="logo-details">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span class="logo_name">CUP UPANG</span>
        
        </div>
        <i class='bx bx-menu' id="btn"></i>
        <ul class="nav-links">
            <li>
                <a href="<?php echo $link_home?>">
                    <i class='bx bxs-dashboard'></i>
                    <span class="link_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_add;?>">
                    <i class="fa-solid fa-money-bill"></i>
                    <span class="link_name">Add Money</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="">
                    <i class="fa-solid fa-coins"></i>
                    <span class="link_name">Pay Fees</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_trans;?>">
                        <i class="fa-solid fa-receipt"></i>
                    <span class="link_name">Transactions</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_settings;?>">
                    <i class='bx bx-cog'></i>
                    <span class="link_name">Settings</span>
                </a>
                <span class="tooltip">Settings</span>
            </li>
        </ul>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <i class='bx bxs-user-circle'></i>
                    <div class="name_job">
                        <div class="name"><?php echo $row['studnum'];?></div>
                        <div class="job">Student</div>
                </div>
                </div>
                <i class='bx bx-log-out' id=log_out></i>
            </div>
            
        </div>
        
    </div>
    <script>
        let btn = document.querySelector("#btn");
        let sidebar = document.querySelector(".sidebar");
        let logout = document.querySelector("#log_out");
        btn.onclick = function(){
            sidebar.classList.toggle("active");
        }
        logout.onclick = function(){
            var r = confirm("Are you sure you want to logout???");
            if(r==true){
                window.location.href = "../../";
            }
        }
    </script>



    <!--#############################################################################################################################################-->
                <!--MODALS-->
    <!--#############################################################################################################################################-->
    <!--#############################################################################################################################################-->
                <!--Tuition Fee Modal-->
    <!--#############################################################################################################################################-->
    <div class="modal fade" id="tuition_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pay Your Tuition Fee</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="paytuition.php" method="POST">
                        <div class="modal-body">
                            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                            <label for="input-group-text" class="form-label">Enter Amount</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text"><i class="fa-solid fa-peso-sign"></i></span>
                                <input type="text" id="" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="" name='amount' onkeypress='return event.charCode >= 48 && event.charCode <= 57' required="required">
                                <!-- <span class="input-group-text">.00</span> -->
                            </div>

                            Tuition Fee Payments:
                            <table style='width: 70%; height: 10px; font-size: 18px; font-weight: 400;'>
                                    <thead></thead>
                                    <tbody>
                                        <tr style="border-bottom: 2px solid; width: 100%; margin-top: 5px; margin-bottom: 4px;">
                                            <td style="width: 45%;">1st Payment:</td>
                                            <td><i class="fa-solid fa-peso-sign"></i></td>
                                            <td style="text-align: right;"><?php echo $pay1;?></td>
                                        </tr>
                                        <tr style="border-bottom: 2px solid; width: 100%;">
                                            <td style="width: 45%;">2nd Payment:</td>
                                            <td><i class="fa-solid fa-peso-sign"></i></td>
                                            <td style="text-align: right;"><?php echo $pay2;?></td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style=>3rd Payment:</td>
                                            <td><i class="fa-solid fa-peso-sign"></i></td>
                                            <td style="text-align: right;"><?php echo $pay3;?></td>
                                        </tr>
                                    </tbody>
                                </table>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success" data-bs-dismiss="modal" name="insert">Confirm</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    <!--#############################################################################################################################################-->
                <!--Other Payments-->
    <!--#############################################################################################################################################-->

    <div class="modal fade" id="other_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pay Other School Activity</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="payothers.php" method="POST">
                        <div class="modal-body">
                            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                            <label for="input-group-text" class="form-label">Please Select an Event</label>
                            <select class="form-select" aria-label="Default select example" required="required" name="event">
                                <?php
                                    $sql="SELECT * FROM tbl_others";
                                    $result = mysqli_query($conn,$sql);
                                    if(mysqli_num_rows($result)===0){
                                        ?>
                                        <option value="">No Events Found</option>
                                        <?php
                                    }else{
                                        while($select=mysqli_fetch_assoc($result)){
                                            ?>
                                                <option value="<?php echo $select['id'];?>"><?php echo $select['eventname'];?>(PHP <?php echo $select['eventprice']?>)</option>
                                            <?php
                                        }
                                    
                                    }
                                    ?>
                            </select>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success" data-bs-dismiss="modal" name="insert">Confirm</button>
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <!--#############################################################################################################################################-->
    <div class="home-section">
        <div class="cardBox">
            <div class="card">
                <div>
                    <div class ="cardName">Your Account Balance is:</div>
                    <div class="numbers"><i class="fa-solid fa-peso-sign"></i><?php echo $balance;?></div>
                    
                </div>
                <!-- <div class="iconBox"><i class='bx bxs-receipt bx-rotate-180'></i></div> -->
            </div>
        </div>
        <?php 
                if(isset($_GET['msg'])){ 
                    if($_GET['msg'] === "success1"){
                        ?><div class="success"><p class="sucess">Transaction Complete... Balance is now Updated</p></div><?php
                    }else if($_GET['msg'] === "low"){
                        ?><div class="error"><p>This CUP Card has Low Balance... Please Add Money to this Account</p></div><?php
                    }else if($_GET['msg'] === "fullpay"){
                        ?><div class="success"><p>Your Tuition Fee is Fully Paid</p></div><?php
                    }else if($_GET['msg'] === "flow"){
                        ?><div class="success"><p>Check Out Complete and has been registered as Transaction..</p></div><?php
                    }else if($_GET['msg'] === "delete"){
                        ?><div class="success"><p>We have updated everything successfully!</p></div><?php
                    }else if($_GET['msg'] === "nochange"){
                        ?><div class="error"><p>Update Failed: No Changes</p></div><?php
                    }else if($_GET['msg'] === "dupli"){
                        ?><div class="error"><p>Duplication Error! Please Delete the other Duplicated Field</p></div><?php
                    }else if($_GET['msg'] === "error"){
                        ?><p class="error">System has Failed to do action!</p><?php
                    }else if($_GET['msg'] === "delerror"){
                        ?><p class="error">System has Failed to delete this record!</p><?php
                    }else{
                        ?><div class="success"><p><?php echo $_GET['msg']?></p></div><?php
                    }
                }?>
        
        <div class="details">
            <div class="loginActivity">
                <div class="loginHeader">
                    <h2>Select Which will you pay<br/></h2>
                    <!-- <button type="button" id="viewtransactions" class="btn btn-primary" style="width: 150px;"><i class="fa-solid fa-eye"></i> View All</button>  -->
                </div>
                <div class="appBox">
                    <button class="app" data-bs-toggle="modal" data-bs-target="#tuition_modal">
                        <div class="appIcon"><i class='bx bx-money'></i></div>
                        <div class="appName">Pay Tuition Fee</div>
                    </button>
                    <button class="app" data-bs-toggle="modal" data-bs-target="#other_modal">
                        <div class="appIcon"><i class='bx bxs-receipt'></i></div>
                        <div class="appName">Pay Other Fees</div>
                    </button>
                </div>
            </div>
            <div class="tuitionFee">
                <div class="tuitionHeader">
                    <h2>Tuition Fee</h2>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td>1st Payment:</td>
                            <td><?php echo $pay1;?></td>
                        </tr>
                        <tr>
                            <td>2nd Payment:</td>
                            <td><?php echo $pay2;?></td>
                        </tr>
                        <tr>
                            <td>3rd Payment:</td>
                            <td><?php echo $pay3;?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script>
        let viewTrans = document.querySelector("#viewtransactions");
        viewTrans.onclick = function(){
                window.location.href = "<?php echo $link_trans;?>";
        }
    </script>
</body>

</html>
<!-- <div class="home-content">
        <div class="overview-boxes">
            <div class="box">
                <div class="left-side">
                    <div class="box_topic">Tenants</div>
                    <div class="number"></div>
                    <div class="indicator">
                        <i class='bx bx-basket'></i>
                    </div>
                </div>
            </div>
        </div>
        </div> -->