<?php
    include "../db_con.php";
    $link_settings = "settings/?id=".$_GET['id'];
    $link_trans = "transactions/?id=".$_GET['id'];
    $link_paynow = "paynow/?id=".$_GET['id'];
    $link_add = "addmoney/?id=".$_GET['id'];
    $sql = "SELECT * FROM tbl_students WHERE id = ".$_GET['id'];
    $result = mysqli_query($conn, $sql); 
    if(mysqli_num_rows($result) === 1){
        $row = mysqli_fetch_assoc($result);
            $admin_lname = $row['lname'];
            $admin_fname = $row['fname'];
            $admin_mi = $row['mi'];
            $admin_studnum = $row['studnum'];
            $balance = $row['balance'];
            $pay1 = $row['pay1'];
            $pay2 = $row['pay2'];
            $pay3 = $row['pay3'];
    }
    if(empty($balance)){
        $balance = "0.00";
    }
    if(empty($pay1)){
        $pay1 = "0.00";
    }
    if(empty($pay2)){
        $pay2 = "0.00";
    }
    if(empty($pay3)){
        $pay3 = "0.00";
    }
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/solid.min.css" integrity="sha512-WTx8wN/JUnDrE4DodmdFBCsy3fTGbjs8aYW9VDbW8Irldl56eMj8qUvI3qoZZs9o3o8qFxfGcyQocUY0LYZqzQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/fontawesome.min.css" integrity="sha512-Rcr1oG0XvqZI1yv1HIg9LgZVDEhf2AHjv+9AuD1JXWGLzlkoKDVvE925qySLcEywpMAYA/rkg296MkvqBF07Yw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="sidebar">
        <div class="logo-details">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span class="logo_name">CUP UPANG</span>
        
        </div>
        <i class='bx bx-menu' id="btn"></i>
        <ul class="nav-links">
            <li>
                <a href="">
                    <i class='bx bxs-dashboard'></i>
                    <span class="link_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_add;?>">
                    <i class="fa-solid fa-money-bill"></i>
                    <span class="link_name">Add Money</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_paynow;?>">
                    <i class="fa-solid fa-coins"></i>
                    <span class="link_name">Pay Fees</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_trans;?>">
                        <i class="fa-solid fa-receipt"></i>
                    <span class="link_name">Transactions</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_settings;?>">
                    <i class='bx bx-cog'></i>
                    <span class="link_name">Settings</span>
                </a>
                <span class="tooltip">Settings</span>
            </li>
        </ul>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <i class='bx bxs-user-circle'></i>
                    <div class="name_job">
                        <div class="name"><?php echo $row['studnum'];?></div>
                        <div class="job">Student</div>
                </div>
                </div>
                <i class='bx bx-log-out' id=log_out></i>
            </div>
            
        </div>
        
    </div>
    <script>
        let btn = document.querySelector("#btn");
        let sidebar = document.querySelector(".sidebar");
        let logout = document.querySelector("#log_out");
        btn.onclick = function(){
            sidebar.classList.toggle("active");
        }
        logout.onclick = function(){
            var r = confirm("Are you sure you want to logout???");
            if(r==true){
                window.location.href = "../";
            }
        }
    </script>
    <div class="home-section">
        <div class="cardBox">
            <div class="card">
                <div>
                    <div class ="cardName">Your Account Balance is:</div>
                    <div class="numbers"><i class="fa-solid fa-peso-sign"></i><?php echo $balance;?></div>
                    
                </div>
                <!-- <div class="iconBox"><i class='bx bxs-receipt bx-rotate-180'></i></div> -->
            </div>
        </div>
        <div class="details">
            <div class="loginActivity">
                <div class="loginHeader">
                    <h2>Transactions<br/></h5></h2><br/>
                    <button type="button" id="viewtransactions" class="btn btn-primary" style="width: 150px;"><i class="fa-solid fa-eye"></i> View All</button> 
                </div>
                <h5>as of <?php echo date('M/d/Y');?> 
                <table>
                    <thead>
                        <tr>
                            <td style="text-align:center;">Description</td>
                            <td style="text-align:center;" colspan=2>Amount</td>
                            <td style="text-align:center;">Time</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sql = "SELECT * FROM tbl_transactions WHERE date = CURRENT_DATE() AND studnum = '".$admin_studnum."' ORDER BY id DESC ";
                            $result = mysqli_query($conn, $sql); 
                            if($result){
                                while($table=mysqli_fetch_assoc($result)){
                                    ?>
                                    <tr>
                                        <td style="text-align:left;"><?php echo $table['desc'];?></td>
                                        <td><i class="fa-solid fa-peso-sign"></i></td>
                                        <td style="text-align:right;"><?php echo $table['amount'];?></td>
                                        <td><?php echo $table['time'];?></td>
                                    </tr>
                                    <?php
                                }
                            }else{
                            }
                            ?>
                    </tbody>
                </table>    
            </div>
            <div class="tuitionFee">
                <div class="tuitionHeader">
                    <h2>Tuition Fee</h2>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td>1st Payment:</td>
                            <td><?php echo $pay1;?></td>
                        </tr>
                        <tr>
                            <td>2nd Payment:</td>
                            <td><?php echo $pay2;?></td>
                        </tr>
                        <tr>
                            <td>3rd Payment:</td>
                            <td><?php echo $pay3;?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script>
        let viewTrans = document.querySelector("#viewtransactions");
        viewTrans.onclick = function(){
                window.location.href = "<?php echo $link_trans;?>";
        }
    </script>
</body>

</html>
<!-- <div class="home-content">
        <div class="overview-boxes">
            <div class="box">
                <div class="left-side">
                    <div class="box_topic">Tenants</div>
                    <div class="number"></div>
                    <div class="indicator">
                        <i class='bx bx-basket'></i>
                    </div>
                </div>
            </div>
        </div>
        </div> -->