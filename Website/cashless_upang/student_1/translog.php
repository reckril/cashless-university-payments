<?php
    include 'database.php';
        $pdo = Database::connect();
        $sql = 'SELECT * FROM tbl_students Where id = "'.$_GET['id'].'"';
        foreach ($pdo->query($sql) as $row){ 
            $studnum = $row['studnum'];
        }
        Database::disconnect();
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<ul>
    <li><a href="<?php echo '../student/?id='.$_GET['id'];?>">Home</a></li>
    <li><a href="<?php echo 'translog.php?id='.$_GET['id'];?>">Transaction Logs</a></li>
    <li><a href="<?php echo 'paynow.php?id='.$_GET['id'];?>">Pay Now</a></li>
    <li><a href="<?php echo 'topup.php?id='.$_GET['id'];?>">Top Up</a></li>
    <li><a href="<?php echo 'accountsettings.php?id='.$_GET['id'];?>">Account Settings</a></li>
    <li><a href="../">Logout</a></li>
    </ul><br/><br/>
<button class="btn">PRINT</button>
<div>
<table style="float: right; width:50%;">
<tr>
    <th colspan=2>TRANSACTION CODES</th>
</tr>
<tr>
    <th>Transaction Code</th>
    <th>Info</th>
</tr>
<tr>
    <th>dep</th>
    <td>Cash Deposit From Teller ON-SITE</td>
</tr>
<tr>
    <th>dpo</th>
    <td>Cash Deposit From ONLINE</td>
</tr>
<tr>
    <th>tui</th>
    <td>Tuition Fee Paid From Teller ON SITE</td>
</tr>
<tr>
    <th>tuo</th>
    <td>Tuition Fee Paid From ONLINE</td>
</tr>
</table>
</div>
<div style="float: left; width:50%;">
<table id="records3">
        <tr>
            <th colspan=3>ALL TIME TRANSACTIONS</th>
        </tr>
        <tr>
            <th>Transaction Code</th>
            <th>Payment Method</th>
            <th>Amount</th>
        </tr>
    
    <?php
        $pdo = Database::connect();
        $sql = 'SELECT * FROM tbl_transactions Where studnum = "'.$studnum.'" ORDER BY date AND time ASC';
        foreach ($pdo->query($sql) as $row){ 
        ?>
        <tr>
            <td style="text-align:center;"><?php echo $row["activity"];?></td>
            <td style="text-align:center;"><?php echo $row["paymethod"];?></td>
            <td style="text-align:right;"><?php echo $row["amount"];?></td>
        </tr>
        <?php
        }
        Database::disconnect();
    ?>
</div>
<script src="js/jquery-3.4.1.js"></script>
             <script>
            $('.btn').click(function(){
            var printme= document.getElementById('records3');
            var wme = window.open("","","width=900,height=700");
            wme.document.write(printme.outerHTML);
            wme.focus();
            wme.print();
            wme.close();
            
        })
</script>

</body>
</html>