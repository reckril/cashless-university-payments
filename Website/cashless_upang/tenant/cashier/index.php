<?php
	$Write="<?php $" . "UIDresult=''; " . "echo $" . "UIDresult;" . " ?>";
	file_put_contents('UIDContainer.php',$Write);
?>
<?php
    include "../db_con.php";
    $link_home=  "../?id=".$_GET['id'];
    $link_products =  "../products/?id=".$_GET['id'];
    $link_trans=  "../transactions/?id=".$_GET['id'];
    $link_settings=  "../settings/?id=".$_GET['id'];
    $sql = "SELECT * FROM tbl_tennants WHERE id = ".$_GET['id'];
    $result = mysqli_query($conn, $sql); 
    if(mysqli_num_rows($result) === 1){
        $row = mysqli_fetch_assoc($result);
            $admin_lname = $row['lname'];
            $admin_fname = $row['fname'];
            $admin_mi = $row['mi'];
    }
    $count_trans = 0;
    $sql = "SELECT * FROM tbl_transactions WHERE date = CURRENT_DATE() and transid ='".$row['franchise']."' ORDER BY time DESC ";
    $result = mysqli_query($conn, $sql);
    $count_trans = mysqli_num_rows($result);

    $count_prod = 0;
    $sql = "SELECT * FROM tbl_products WHERE franchise = '".$row['franchise']."'";
    $result = mysqli_query($conn, $sql);
    $count_prod = mysqli_num_rows($result);
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/solid.min.css" integrity="sha512-WTx8wN/JUnDrE4DodmdFBCsy3fTGbjs8aYW9VDbW8Irldl56eMj8qUvI3qoZZs9o3o8qFxfGcyQocUY0LYZqzQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/fontawesome.min.css" integrity="sha512-Rcr1oG0XvqZI1yv1HIg9LgZVDEhf2AHjv+9AuD1JXWGLzlkoKDVvE925qySLcEywpMAYA/rkg296MkvqBF07Yw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="sidebar">
        <div class="logo-details">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span class="logo_name">CUP UPANG</span>
        
        </div>
        <i class='bx bx-menu' id="btn"></i>
        <ul class="nav-links">
            <li>
                <a href="<?php echo $link_home?>">
                    <i class='bx bxs-dashboard'></i>
                    <span class="link_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_products?>">
                    <i class="fa-solid fa-warehouse"></i> 
                    <span class="link_name">Inventory</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="">
                <i class="fa-solid fa-cash-register"></i>
                    <span class="link_name">Cashier</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_trans;?>">
                        <i class="fa-solid fa-receipt"></i>
                    <span class="link_name">Transactions</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_settings;?>">
                    <i class='bx bx-cog'></i>
                    <span class="link_name">Settings</span>
                </a>
                <span class="tooltip">Settings</span>
            </li>
        </ul>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <i class='bx bxs-user-circle' ></i>
                    <div class="name_job">
                        <div class="name"><?php echo $admin_lname.", ".$admin_fname." ".$admin_mi.".";?></div>
                        <div class="job">Teller</div>
                </div>
                </div>
                <i class='bx bx-log-out' id=log_out></i>
            </div>
            
        </div>
        
    </div>
    <script>
        let btn = document.querySelector("#btn");
        let sidebar = document.querySelector(".sidebar");
        let logout = document.querySelector("#log_out");
        btn.onclick = function(){
            sidebar.classList.toggle("active");
        }
        logout.onclick = function(){
            var r = confirm("Are you sure you want to logout???");
            if(r==true){
                window.location.href = "../../";
            }
        }
    </script>
    <div class="home-section">
        <div class="cardBox">
            <div class="card">
                <div>
                    <div class="numbers"><?php echo $count_trans;?></div>
                    <div class ="cardName">Transactions Today</div>
                </div>
                <div class="iconBox"><i class='bx bxs-receipt bx-rotate-180'></i></div>
            </div>
            <div class="card">
                <div>
                    <div class="numbers"><?php echo $count_prod;?></div>
                    <div class ="cardName">Products</div>
                </div>
                <div class="iconBox"><i class='bx bxs-basket'></i></div>
            </div>
        </div>

        <div class="modal fade" id="deposit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Reload CUP CARD</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="deposit.php" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                        <div class="mb-3" style="padding: 1px 20px;">
                            <label for="exampleFormControlTextarea1" class="form-label">CUP NUMBER</label>
                            <textarea class="form-control getUID" id="exampleFormControlTextarea1" rows="1" cols="1" required="required" name="rfidnum" style="resize: none;"></textarea>
                            <h6>Tap the Card to the Machine</h6>
                        </div>
                        
                        <div class="input-group mb-3">
                            <span class="input-group-text"><i class="fa-solid fa-peso-sign"></i></span>
                            <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" placeholder="Minimum of 50 pesos" name='amount' onkeypress='return event.charCode >= 48 && event.charCode <= 57' required="required">
                            <span class="input-group-text">.00</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" name="insert">Deposit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addcart" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Item to Cart</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="cart.php" method="POST">
                        <div class="modal-body">
                            <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">Quantity:</label>
                                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="" name="qty">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Product</label>
                                <select class="form-select" aria-label="Default select example" name="selected">
                            <?php
                                $sql="SELECT * FROM tbl_products WHERE franchise='".$row['franchise']."'";
                                $result = mysqli_query($conn,$sql);
                                if(mysqli_num_rows($result) > 0){
                                    while($select = mysqli_fetch_assoc($result)){
                                    ?>
                                    <option value="<?php echo $select['id'];?>"><?php echo $select['prodname'];?></option>
                                    <?php
                                    }
                                }else{
                                ?>
                                <option value="">No Products Registered</option>
                                <?php
                                }   
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" name="insert">Add to Cart</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="paycash" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Pay Using CUP Card</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="paycup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Checkout using CUP Card</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="checkOutCup.php" method="post">
                    <div class="modal-body">
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                        <div class="mb-3" style="padding: 1px 20px;">
                            <label for="exampleFormControlTextarea1" class="form-label">CUP NUMBER</label>
                            <textarea class="form-control getUID" id="exampleFormControlTextarea1" rows="1" cols="1" required="required" name="rfidnum" style="resize: none;"></textarea>
                            <h6>Tap the Card to the Machine</h6>
                        </div>
                        <div class="mb-3">
                            <label for="formGroupExampleInput" class="form-label">Total Amount</label>
                            <input type="text" class="form-control updateTotal" id="formGroupExampleInput" name="amount" placeholder="" readonly="readonly">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" name="insert"><i class="fa-solid fa-cash-register"></i>  Check Out</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="delmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Remove Items in Cart</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form action="deleteData.php" method="POST">
                        <div class="modal-body">
                            Are You Sure You Want to Remove this item to the Cart?
                                <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                                <input type="hidden" name="placing" id="delete_id">
                        </div>       
                        <div class="modal-footer">
                                <button type="submit" class="btn btn-success" name="insert">Yes</button>
                                <button type="button" class="btn btn-danger delbtn" data-bs-dismiss="modal">No</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php 
                if(isset($_GET['msg'])){ 
                    if($_GET['msg'] === "dep"){
                        ?><div class="success"><p class="sucess">We have Successfully Reload The CUP CARD!</p></div><?php
                    }else if($_GET['msg'] === "low"){
                        ?><div class="error"><p>This CUP Card has Low Balance... Please Reload CUP Card to Continue</p></div><?php
                    }else if($_GET['msg'] === "user2error"){
                        ?><div class="error"><p>The Username has already been taken please check what you are editing!</p></div><?php
                    }else if($_GET['msg'] === "flow"){
                        ?><div class="success"><p>Check Out Complete and has been registered as Transaction..</p></div><?php
                    }else if($_GET['msg'] === "delete"){
                        ?><div class="success"><p>We have updated everything successfully!</p></div><?php
                    }else if($_GET['msg'] === "nochange"){
                        ?><div class="error"><p>Update Failed: No Changes</p></div><?php
                    }else if($_GET['msg'] === "dupli"){
                        ?><div class="error"><p>Duplication Error! Please Delete the other Duplicated Field</p></div><?php
                    }else if($_GET['msg'] === "error"){
                        ?><p class="error">System has Failed to do action!</p><?php
                    }else if($_GET['msg'] === "delerror"){
                        ?><p class="error">System has Failed to delete this record!</p><?php
                    }else{
                        ?><div class="success"><p><?php echo $_GET['msg']?></p></div><?php
                    }
                }?>
        <div class="details">
        <div class="appDex">
                <div class="appHeader">
                <h2>Add Money via:</h2>
                </div>
                <div class="appBox">
                <button class="app" data-bs-toggle="modal" data-bs-target="#deposit">
                    <div class="appIcon"><i class="fa-solid fa-credit-card"></i></div>
                    <div class="appName">Reload Cup Card</div>
                </button>
                <button class="app" data-bs-toggle="modal" data-bs-target="#addcart">
                    <div class="appIcon"><i class="fa-solid fa-cart-arrow-down"></i></div>
                    <div class="appName">Add An Item to Customer Cart</div>
                </button>
            </div>
            </div>
            <div class="emptySpace">
                <div class="whitespace">
                    <div>
                        <h2>Customer's Cart</h2>
                    </div>
                <table class="table table-striped" style="overflow: scroll;">
                    <thead>
                        <tr>
                            <td>QTY</td>
                            <td>Product</td>
                            <td colspan=2 style="text-align:center;">Regular Price</td>
                            <td colspan=2 style="text-align:center;">Total Price</td>
                            <td>Action</td>
                        </tr>       
                    </thead>
                    <tbody>
                        <?php 
                            $count = 0.00;
                            $total = 0.00;
                            $sql="SELECT * FROM tbl_cart WHERE seller_id=".$_GET['id'];
                            $result = mysqli_query($conn,$sql);
                            while($row=mysqli_fetch_assoc($result)){
                                $string_total = str_replace(',','',$row['total']);
                                $total = (($string_total + 0.01) - 0.01); 
                                $count = $count + $total;
                                ?>
                                <tr>
                                    <td hidden><?php echo $row['id'];?></td>
                                    <td style="text-align:center;"><?php echo $row['qty'];?></td>
                                    <td><?php echo $row['prodname'];?></td>
                                    <td><i class="fa-solid fa-peso-sign"></i></td>
                                    <td><?php echo $row['price'];?></td>
                                    <td><i class="fa-solid fa-peso-sign"></i></td>
                                    <td><?php echo $row['total'];?></td>
                                    <td style="align-items: center;">
                                        <button type="button" class="btn btn-danger deletebtn"><i class='bx bxs-trash'></i></button>
                                    </td>
                                </tr>
                                <?php
                            }
                            $string_count = number_format($count,2,'.',',');
                        ?>
                    </tbody>
                </table>
                <div><h4>Overall Total: <i class="fa-solid fa-peso-sign"></i><span id="overalls"><?php echo $string_count;?></span></h4></div>
                <input type="text" id="total" value="<?php echo $string_count;?>" hidden>
                <!-- <button type="button" class="btn btn-primary" id="putcash">Pay by Cash</button> -->
                <button type="button" class="btn btn-success" id="putcup">Pay by using CUP Card</button>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>                    
        <script>
			$(document).ready(function(){
				 $(".getUID").load("UIDContainer.php");
				setInterval(function() {
					$(".getUID").load("UIDContainer.php");
				}, 500);
			});
        </script>
        
    </div>
    <script>
        $(document).ready(function(){
            $('.deletebtn').on('click', function(){
                    $('#delmodal').modal('show');
                    $tr = $(this).closest('tr');

                        var data = $tr.children("td").map(function() {
                            return $(this).text();
                        }).get();
                    console.log(data)
                    var id = data[0].trim();
                    $('#delete_id').val(id);
            });
        });
    </script>
    <script>
        let total = document.querySelector('#total');
        $(document).ready(function(){
                $('#putcup').on('click', function(){
                    $('#paycup').modal('show');
                    $('.updateTotal').val(total.value);
                });
            });
    </script>
</body>

</html>
<!-- <div class="home-content">
        <div class="overview-boxes">
            <div class="box">
                <div class="left-side">
                    <div class="box_topic">Tenants</div>
                    <div class="number"></div>
                    <div class="indicator">
                        <i class='bx bx-basket'></i>
                    </div>
                </div>
            </div>
        </div>
        </div> -->