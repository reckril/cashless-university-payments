<?php
    include '../db_con.php';
    $array = $_POST['fillsearch'];
    // echo json_encode($array);
    $search = $array[0];
    $to_date = $array[1];
    $from_date = $array[2];
    $franchise = $array[3];
    $time_from = strtotime($from_date);
    $newformat_from = date('Y-m-d',$time_from);
    // echo $newformat_from;
    $time_to = strtotime($to_date);
    $newformat_to = date('Y-m-d',$time_to);
    // echo $search;
    if(empty($search)){
        $result7 = $conn->query("SELECT *, Month(date) AS 'Month', Day(date) AS 'Day', Year(date) AS 'Year', Hour(time)  AS 'Hour', Minute(time)  AS 'Minute'  FROM tbl_transactions WHERE transid ='".$franchise."' AND date between '".$newformat_from."' AND '".$newformat_to."' ORDER BY id DESC");
        $transactions = $result7 -> fetch_all(MYSQLI_ASSOC);
        ?>
        <table class="display table table-striped" style="width:100%; height: 5px;">
                    <thead>
                        <tr>
                            <td>Transaction Number</td>
                            <td>Student Number</td>
                            <td style="text-align: center;">Description</td>
                            <td style="text-align: center;" colspan=2>Amount</td>
                            <td style="text-align: center;">Date</td>
                            <td style="text-align: center;">Time</td>
                        </tr>
                    </thead>
                    <tbody>
        <?php
        foreach($transactions as $transaction):
            if($transaction['Month'] == 1){
                $month = "January";
            }else if($transaction['Month'] == 2){
                $month = "February";
            }else if($transaction['Month'] == 3){
                $month = "March";
            }else if($transaction['Month'] == 4){
                $month = "April";
            }else if($transaction['Month'] == 5){
                $month = "May";
            }else if($transaction['Month'] == 6){
                $month = "June";
            }else if($transaction['Month'] == 7){
                $month = "July";
            }else if($transaction['Month'] == 8){
                $month = "August";
            }else if($transaction['Month'] == 9){
                $month = "September";
            }else if($transaction['Month'] == 10){
                $month = "October";
            }else if($transaction['Month'] == 11){
                $month = "November";
            }else if($transaction['Month'] == 12){
                $month = "December";
            }

            if($transaction['Hour'] <= 11 && $transaction['Hour'] != 0){
                                if($transaction['Minute'] < 10){
                                    $time = $transaction['Hour'].':0'.$transaction['Minute'].' A.M.';
                                }else{
                                    $time = $transaction['Hour'].':'.$transaction['Minute'].' A.M.';
                                }
                            }else if($transaction['Hour'] == 0){
                                if($transaction['Minute'] < 10){
                                    $time = '12:0'.$transaction['Minute'].' A.M.';
                                }else{
                                    $time = '12:'.$transaction['Minute'].' A.M.';
                                }
                            }else if($transaction['Hour'] == 12){
                                if($transaction['Minute'] < 10){
                                    $time = '12:0'.$transaction['Minute'].' P.M.';
                                }else{
                                    $time = '12:'.$transaction['Minute'].' P.M.';
                                }
                            }else{
                                $hourconv = $transaction['Hour'] - 12;
                                if($transaction['Minute'] < 10){
                                    $time = $time = $hourconv.':0'.$transaction['Minute'].' P.M.';;
                                }else{
                                    $time = $time = $hourconv.':'.$transaction['Minute'].' P.M.';;
                                }
                            }
                            ?>
                            <tr>
                                <td style="vertical-align: top; width: 15px;"><?= $transaction['transnumber'];?></td>
                                <td style="text-align: left; vertical-align: top; width: 140px;"><?= $transaction['studnum'];?></td>
                                <td stlye="overflow: hidden; text-align: left; vertical-align: top;"><?= $transaction['desc'];?></td>
                                <td style="text-align: left; vertical-align: top;"><i class="fa-solid fa-peso-sign"></i></td>
                                <td style="text-align: right;"><?php echo number_format((float)$transaction['amount'], 2, '.', ',');?></td>
                                <td style="text-align: center; width: 165px;"><?php echo $month.' '.$transaction['Day'].', '.$transaction['Year'];?></td>
                                <td style="text-align: center; width: 93px;"><?php echo $time;?></td>
                            </tr>
                            
                            <?php
                endforeach;
                ?>
        
        </tbody>
        </table>
        <?php
    }else{
        $result7 = $conn->query("SELECT *, Month(date) AS 'Month', Day(date) AS 'Day', Year(date) AS 'Year', Hour(time)  AS 'Hour', Minute(time)  AS 'Minute'  FROM tbl_transactions WHERE transid ='".$franchise."' AND date between '".$newformat_from."' AND '".$newformat_to."' AND (studnum LIKE '%".$search."%' OR transnumber LIKE '%".$search."%') ORDER BY id DESC");
        $transactions = $result7 -> fetch_all(MYSQLI_ASSOC);
        ?>
        <table class="display table table-striped" style="width:100%; height: 5px;">
                    <thead>
                        <tr>
                            <td>Transaction Number</td>
                            <td>Student Number</td>
                            <td style="text-align: center;">Description</td>
                            <td style="text-align: center;" colspan=2>Amount</td>
                            <td style="text-align: center;">Date</td>
                            <td style="text-align: center;">Time</td>
                        </tr>
                    </thead>
                    <tbody>
        <?php
        foreach($transactions as $transaction):
            if($transaction['Month'] == 1){
                $month = "January";
            }else if($transaction['Month'] == 2){
                $month = "February";
            }else if($transaction['Month'] == 3){
                $month = "March";
            }else if($transaction['Month'] == 4){
                $month = "April";
            }else if($transaction['Month'] == 5){
                $month = "May";
            }else if($transaction['Month'] == 6){
                $month = "June";
            }else if($transaction['Month'] == 7){
                $month = "July";
            }else if($transaction['Month'] == 8){
                $month = "August";
            }else if($transaction['Month'] == 9){
                $month = "September";
            }else if($transaction['Month'] == 10){
                $month = "October";
            }else if($transaction['Month'] == 11){
                $month = "November";
            }else if($transaction['Month'] == 12){
                $month = "December";
            }

            if($transaction['Hour'] <= 11 && $transaction['Hour'] != 0){
                                if($transaction['Minute'] < 10){
                                    $time = $transaction['Hour'].':0'.$transaction['Minute'].' A.M.';
                                }else{
                                    $time = $transaction['Hour'].':'.$transaction['Minute'].' A.M.';
                                }
                            }else if($transaction['Hour'] == 0){
                                if($transaction['Minute'] < 10){
                                    $time = '12:0'.$transaction['Minute'].' A.M.';
                                }else{
                                    $time = '12:'.$transaction['Minute'].' A.M.';
                                }
                            }else if($transaction['Hour'] == 12){
                                if($transaction['Minute'] < 10){
                                    $time = '12:0'.$transaction['Minute'].' P.M.';
                                }else{
                                    $time = '12:'.$transaction['Minute'].' P.M.';
                                }
                            }else{
                                $hourconv = $transaction['Hour'] - 12;
                                if($transaction['Minute'] < 10){
                                    $time = $time = $hourconv.':0'.$transaction['Minute'].' P.M.';;
                                }else{
                                    $time = $time = $hourconv.':'.$transaction['Minute'].' P.M.';;
                                }
                            }
                            ?>
                            <tr>
                                <td style="vertical-align: top; width: 15px;"><?= $transaction['transnumber'];?></td>
                                <td style="text-align: left; vertical-align: top; width: 140px;"><?= $transaction['studnum'];?></td>
                                <td stlye="overflow: hidden; text-align: left; vertical-align: top;"><?= $transaction['desc'];?></td>
                                <td style="text-align: left; vertical-align: top;"><i class="fa-solid fa-peso-sign"></i></td>
                                <td style="text-align: right;"><?php echo number_format((float)$transaction['amount'], 2, '.', ',');?></td>
                                <td style="text-align: center; width: 165px;"><?php echo $month.' '.$transaction['Day'].', '.$transaction['Year'];?></td>
                                <td style="text-align: center; width: 93px;"><?php echo $time;?></td>
                            </tr>
                            
                            <?php
                endforeach;
                ?>
        
        </tbody>
        </table>
        <?php
    }
?>