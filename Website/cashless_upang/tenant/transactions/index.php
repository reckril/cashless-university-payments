<?php
    include "../db_con.php";
    $link_settings = "../settings/?id=".$_GET['id'];
    $link_home = "../?id=".$_GET['id'];
    $link_products = "../products/?id=".$_GET['id'];
    $link_cashier = "../cashier/?id=".$_GET['id'];
    $message = "";
    $sql = "SELECT * FROM tbl_tennants WHERE id = ".$_GET['id'];
    $result = mysqli_query($conn, $sql); 
    if(mysqli_num_rows($result) === 1){
        $row = mysqli_fetch_assoc($result);
        $admin_lname = $row['lname'];
        $admin_fname = $row['fname'];
        $admin_mi = $row['mi'];
        // $balance = $row['balance'];
        // $pay1 = $row['pay1'];
        // $pay2 = $row['pay2'];
        // $pay3 = $row['pay3'];
}
    
    if(isset($_GET['from_date']) && isset($_GET['to_date'])){
        $time_from = strtotime($_GET['from_date']);
        $newformat_from = date('Y-m-d',$time_from);
        // echo $newformat_from;

        $time_to = strtotime($_GET['to_date']);
        $newformat_to = date('Y-m-d',$time_to);
        
        $secs = $time_to - $time_from;
        $days = $secs / 86400;
        if($days <= -1){
            $message = "incorrect_date_diff";
            $result7 = $conn->query("SELECT *, Month(date) AS 'Month', Day(date) AS 'Day', Year(date) AS 'Year', Hour(time)  AS 'Hour', Minute(time)  AS 'Minute'  FROM tbl_transactions WHERE transid ='".$row['franchise']."' ORDER BY id DESC");
            $transactions = $result7 -> fetch_all(MYSQLI_ASSOC);
        }else{
            $result7 = $conn->query("SELECT *, Month(date) AS 'Month', Day(date) AS 'Day', Year(date) AS 'Year', Hour(time)  AS 'Hour', Minute(time)  AS 'Minute'  FROM tbl_transactions WHERE transid ='".$row['franchise']."' AND date between '".$newformat_from."' AND '".$newformat_to."' ORDER BY id DESC");
            $transactions = $result7 -> fetch_all(MYSQLI_ASSOC);
        }
    }else{
        $result7 = $conn->query("SELECT *, Month(date) AS 'Month', Day(date) AS 'Day', Year(date) AS 'Year', Hour(time)  AS 'Hour', Minute(time)  AS 'Minute'  FROM tbl_transactions WHERE transid ='".$row['franchise']."' ORDER BY id DESC");
        $transactions = $result7 -> fetch_all(MYSQLI_ASSOC);
    }
    // $result7 = $conn->query("SELECT *, Month(date) AS 'Month', Day(date) AS 'Day', Year(date) AS 'Year', Hour(time)  AS 'Hour', Minute(time)  AS 'Minute'  FROM tbl_transactions WHERE (transid = 'STUDENT' OR paymethod = 'TELLER' ) ORDER BY id DESC LIMIT $start, $limit");
    // $transactions = $result7 -> fetch_all(MYSQLI_ASSOC);

?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/solid.min.css" integrity="sha512-WTx8wN/JUnDrE4DodmdFBCsy3fTGbjs8aYW9VDbW8Irldl56eMj8qUvI3qoZZs9o3o8qFxfGcyQocUY0LYZqzQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/fontawesome.min.css" integrity="sha512-Rcr1oG0XvqZI1yv1HIg9LgZVDEhf2AHjv+9AuD1JXWGLzlkoKDVvE925qySLcEywpMAYA/rkg296MkvqBF07Yw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
</head>
<body>
<div class="sidebar">
        <div class="logo-details">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span class="logo_name">CUP UPANG</span>
        
        </div>
        <i class='bx bx-menu' id="btn"></i>
        <ul class="nav-links">
            <li>
                <a href="<?php echo $link_home?>">
                    <i class='bx bxs-dashboard'></i>
                    <span class="link_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_products?>">
                    <i class="fa-solid fa-warehouse"></i> 
                    <span class="link_name">Product</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_cashier?>">
                <i class="fa-solid fa-cash-register"></i>
                    <span class="link_name">Cashier</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="">
                        <i class="fa-solid fa-receipt"></i>
                    <span class="link_name">Transactions</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_settings?>">
                    <i class='bx bx-cog'></i>
                    <span class="link_name">Settings</span>
                </a>
                <span class="tooltip">Settings</span>
            </li>
        </ul>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <i class='bx bxs-user-circle' ></i>
                    <div class="name_job">
                        <div class="name"><?php echo $admin_fname;?></div>
                        <div class="job">Teller</div>
                </div>
                </div>
                <i class='bx bx-log-out' id=log_out></i>
            </div>
            
        </div>
        
    </div>
    <script>
        let btn = document.querySelector("#btn");
        let sidebar = document.querySelector(".sidebar");
        let logout = document.querySelector("#log_out");
        btn.onclick = function(){
            sidebar.classList.toggle("active");
        }
        logout.onclick = function(){
            var r = confirm("Are you sure you want to logout???");
            if(r==true){
                window.location.href = "../../";
            }
        }
    </script>
    <div class="home-section">
        <input type="hidden" value="<?php echo $row['franchise'];?>" id="franchising">
        <div class="container">
            <div style="width:100%; text-align: center;"><h2><?php echo $row['franchise'];?> Transactions</h2></div>
            <div class="jumbotron">
                <?php
                    if(!empty($message)){
                        if($message === "incorrect_date_diff"){
                            ?>
                            <div class="error"><p>Error: Your Date Filter isn't the right... Please Make Sure to put the right From and To Date</p></div>
                            <?php
                        }
                    }
                ?>
            <div>
            <form action="" method="GET">
                    <div class="row">
                        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                        <div class="col-md-3">
                            <div class="form-group">
                                        <label>From Date</label>
                                        <input id="from" type="date" name="from_date" value="<?php if(isset($_GET['from_date'])){ echo $_GET['from_date']; } ?>" class="form-control" required="required">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>To Date</label>
                                        <input id="to" type="date" name="to_date" value="<?php if(isset($_GET['to_date'])){ echo $_GET['to_date']; } ?>" class="form-control" required="required">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Click to Filter</label> <br>
                                        <button type="submit" class="btn btn-primary">Filter</button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Click to Clear All Filters</label> <br>
                                        <button type="button" onclick="clearData()" class="btn btn-danger">Clear all Filter</button>
                                    </div>
                                </div>
                    </div>
                </form>
                <script>
                    function clearData(){
                        location.href = "<?php echo 'index.php?id='.$_GET['id']?>";
                    }
                </script>
                <?php 
                    if(isset($_GET['from_date']) && isset($_GET['to_date'])){
                        ?>
                        
                        <div class="search">
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="inputGroup-sizing-default">Search Student No. or Trans Number:</span>
                                <input type="text" class="form-control" id="live_search" autocomplete="off" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" style="width: 40px;">
                            </div>
                        </div>
                        
                        <?php
                    }else{
                ?>
                <div class="search">
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-default">Search Student No. or Trans Number:</span>
                        <input type="text" class="form-control" id="live_search1" autocomplete="off" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" style="width: 40px;">
                    </div>
                </div>
                <?php
                    }
                ?>
            <div id="search_result" style ="max-height: 425px; overflow: auto;">
            <table id="example" class="display table table-striped" style="width:100%; height: 5px;">
                    <thead>
                        <tr>
                            <td>Transaction Number</td>
                            <td>Student Number</td>
                            <td style="text-align: center;">Description</td>
                            <td style="text-align: center;" colspan=2>Amount</td>
                            <td style="text-align: center;">Date</td>
                            <td style="text-align: center;">Time</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            foreach($transactions as $transaction) :
                            // Month Conversion
                            if($transaction['Month'] == 1){
                                $month = "January";
                            }else if($transaction['Month'] == 2){
                                $month = "February";
                            }else if($transaction['Month'] == 3){
                                $month = "March";
                            }else if($transaction['Month'] == 4){
                                $month = "April";
                            }else if($transaction['Month'] == 5){
                                $month = "May";
                            }else if($transaction['Month'] == 6){
                                $month = "June";
                            }else if($transaction['Month'] == 7){
                                $month = "July";
                            }else if($transaction['Month'] == 8){
                                $month = "August";
                            }else if($transaction['Month'] == 9){
                                $month = "September";
                            }else if($transaction['Month'] == 10){
                                $month = "October";
                            }else if($transaction['Month'] == 11){
                                $month = "November";
                            }else if($transaction['Month'] == 12){
                                $month = "December";
                            }
                            if($transaction['Hour'] <= 11 && $transaction['Hour'] != 0){
                                if($transaction['Minute'] < 10){
                                    $time = $transaction['Hour'].':0'.$transaction['Minute'].' A.M.';
                                }else{
                                    $time = $transaction['Hour'].':'.$transaction['Minute'].' A.M.';
                                }
                            }else if($transaction['Hour'] == 0){
                                if($transaction['Minute'] < 10){
                                    $time = '12:0'.$transaction['Minute'].' A.M.';
                                }else{
                                    $time = '12:'.$transaction['Minute'].' A.M.';
                                }
                            }else if($transaction['Hour'] == 12){
                                if($transaction['Minute'] < 10){
                                    $time = '12:0'.$transaction['Minute'].' P.M.';
                                }else{
                                    $time = '12:'.$transaction['Minute'].' P.M.';
                                }
                            }else{
                                $hourconv = $transaction['Hour'] - 12;
                                if($transaction['Minute'] < 10){
                                    $time = $time = $hourconv.':0'.$transaction['Minute'].' P.M.';;
                                }else{
                                    $time = $time = $hourconv.':'.$transaction['Minute'].' P.M.';;
                                }
                            }
                            ?>
                            <tr>
                                <td style="vertical-align: top; width: 15px;"><?= $transaction['transnumber'];?></td>
                                <td style="text-align: left; vertical-align: top; width: 140px;"><?= $transaction['studnum'];?></td>
                                <td stlye="overflow: hidden; text-align: left; vertical-align: top;"><?= $transaction['desc'];?></td>
                                <td style="text-align: left; vertical-align: top;"><i class="fa-solid fa-peso-sign"></i></td>
                                <td style="text-align: right;"><?php echo number_format((float)$transaction['amount'], 2, '.', ',');?></td>
                                <td style="text-align: center; width: 165px;"><?php echo $month.' '.$transaction['Day'].', '.$transaction['Year'];?></td>
                                <td style="text-align: center; width: 90px;"><?php echo $time;?></td>
                            </tr>
                            <?php
                            endforeach;
                        ?>
                    </tbody>
                        <!-- <tfoot>
                            <tr>
                                <td style="text-align: center;">Transaction Number</td>
                                <td>Student Number</td>
                                <td style="text-align:center;">Description</td>
                                <td style="text-align: center;">Amount</td>
                                <td style="text-align: center;">Date</td>
                                <td style="text-align: center;">Time</td>
                            </tr>
                        </tfoot> -->
                </table>
            </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        

        <script type="text/javascript">
            $(document).ready(function(){

                $("#live_search").keyup(function(){
                    var input = $(this).val();
                    var to = $('#to').val();
                    var from = $('#from').val();
                    var franchise = $('#franchising').val();
                    $.ajax({
                        url: "search.php",
                        method: "POST",
                        data:{fillsearch:[input,to,from,franchise]},

                        success:function(data){
                            $("#search_result").html(data);
                        }
                    })
                })

                $("#live_search1").keyup(function(){
                    var input = $(this).val();
                    var franchise = $('#franchising').val();
                    $.ajax({
                        url: "searchnodate.php",
                        method: "POST",
                        data:{fillsearch:[input,franchise]},

                        success:function(data){
                            $("#search_result").html(data);
                        }
                    })
                })
            });
        </script>   
      
    </div>
</body>
</html>
