<?php
    include "../db_con.php";
    
    if(isset($_POST['insert'])){
        $ad_id = $_POST['id'];
        $st_id = $_POST['placing'];
        $sql = "SELECT * FROM tbl_students WHERE id =".$st_id;
        $result = mysqli_query($conn, $sql);
        if(mysqli_num_rows($result) === 1){
            $row = mysqli_fetch_assoc($result);
            $stud_total = ($row['pay1'] + $row['pay2'] + $row['pay3'] + 0.01) - 0.01;
        }
        $lname = $_POST['lname'];
        $fname = $_POST['fname'];
        $mi = $_POST['mi'];
        $studnum = $_POST['studnum'];
        $email = $_POST['email'];
        $curr_rfid = $_POST['curr_rfidnum'];
        $rfidnum = str_replace(" ","",$_POST['rfidnum']);
        $total = $_POST['total'];
        $string_total = str_replace(',','',$total);
        $pay2 = $pay1 = $string_total * 0.35;
        $pay3 = $string_total * 0.3;
        $pword="";
        // echo $studnum;
        if(isset($_POST['defpass'])){
            $pword = strtoupper($lname);
        }else{
            $pword = $row['pword'];
        }
        $sql = "SELECT * FROM tbl_students WHERE studnum = '".$studnum."'";
        $result = mysqli_query($conn, $sql);
        if(mysqli_num_rows($result) === 1){
            $check = mysqli_fetch_assoc($result);
            if($check['lname'] === $lname && $check['fname'] === $fname && $check['mi'] === $mi && $check['email'] === $email && $check['studnum'] === $studnum && empty($rfidnum) && $stud_total === $string_total){
                header("Location: index.php?id=".$ad_id."&msg=nochange");
                exit();
            }else{
                if(empty($rfidnum)){
                    if($check['id'] === $row['id']){
                        $sql = "UPDATE tbl_students SET lname='".$lname."', fname='".$fname."', mi='".$mi."', email='".$email."', pword='".$pword."', pay1='".$pay1."', pay2='".$pay2."', pay3='".$pay3."' WHERE id=".$st_id;
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            header("Location: index.php?id=".$ad_id."&msg=update");
                            exit();
                        }else{
                            header("Location: index.php?id=".$ad_id."&msg=error");
                            exit();
                        }
                    }else{
                        if($check['studnum'] === $studnum){
                            header("Location: index.php?id=".$ad_id."&msg=studnum2error");
                            exit();
                        }
                    }
                }else{
                    $sql = "SELECT rfidnum FROM tbl_students WHERE rfidnum = '".$rfidnum."'";
                    $result = mysqli_query($conn, $sql);
                    if(mysqli_num_rows($result) > 0){
                        header("Location: index.php?id=".$ad_id."&msg=rfiderror");
                        exit();
                    }else{
                        if($check['id'] === $row['id']){
                            $sql = "UPDATE tbl_students SET lname='".$lname."', fname='".$fname."', mi='".$mi."', email='".$email."', pword='".$pword."', rfidnum='".$rfidnum."', pay1='".$pay1."', pay2='".$pay2."', pay3='".$pay3."' WHERE id=".$st_id;
                            $result = mysqli_query($conn,$sql);
                            if($result){
                                header("Location: index.php?id=".$ad_id."&msg=update");
                                exit();
                            }else{
                                header("Location: index.php?id=".$ad_id."&msg=error");
                                exit();
                            }
                        }else{
                            if($check['studnum'] === $studnum){
                                header("Location: index.php?id=".$ad_id."&msg=studnum2error");
                                exit();
                            }
                        }
                    }
                }
            }
        }else if(mysqli_num_rows($result) === 0){
            if(empty($rfidnum)){
                if($check['id'] === $row['id']){
                    $sql = "UPDATE tbl_students SET lname='".$lname."', fname='".$fname."', mi='".$mi."', email='".$email."', pword='".$pword."' WHERE id=".$st_id;
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        header("Location: index.php?id=".$ad_id."&msg=update");
                        exit();
                    }else{
                        header("Location: index.php?id=".$ad_id."&msg=error");
                        exit();
                    }
                }else{
                    if($check['studnum'] === $studnum){
                        header("Location: index.php?id=".$ad_id."&msg=studnum2error");
                        exit();
                    }
                }
            }else{
                $sql = "SELECT rfidnum FROM tbl_students WHERE rfidnum = ".$rfidnum;
                $result = mysqli_query($conn, $sql);
                if(mysqli_num_rows($result) > 0){
                    header("Location: index.php?id=".$ad_id."&msg=rfiderror");
                    exit();
                }else{
                    if($check['id'] === $row['id']){
                        $sql = "UPDATE tbl_students SET lname='".$lname."', fname='".$fname."', mi='".$mi."', email='".$email."', pword='".$pword."', rfidnum='".$rfidnum."' WHERE id=".$st_id;
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            header("Location: index.php?id=".$ad_id."&msg=update");
                            exit();
                        }else{
                            header("Location: index.php?id=".$ad_id."&msg=error");
                            exit();
                        }
                    }else{
                        if($check['studnum'] === $studnum){
                            header("Location: index.php?id=".$ad_id."&msg=studnum2error");
                            exit();
                        }
                    }
                }
            }
        }else{
            header("Location: index.php?id=".$ad_id."&msg=dupli");
            exit();
        }
    }else{
        header("Location: index.php?id=".$ad_id."&msg=error");
        exit();
    }
?>