<?php

include '../db_con.php';
    if(isset($_POST['insert'])){
        $ad_id = $_POST['id'];
        $st_id = $_POST['placing'];
        $rfidnum = $_POST['rfidnum'];
        $count_sql = "SELECT * FROM tbl_transactions";
        $count_result = mysqli_query($conn,$count_sql);
        $count1 = mysqli_num_rows($count_result) + 1;
        $sql = "SELECT * FROM tbl_accounts WHERE id=".$ad_id;
        $result = mysqli_query($conn,$sql);
        $user = "";
        if(mysqli_num_rows($result) === 1){
            $tell=mysqli_fetch_assoc($result);
            $user = $tell['username'];
        }
        if(empty($rfidnum)){
            header('Location: index.php?id='.$ad_id.'&msg=norfid');
            exit();
        }else{
            $sql = "SELECT * FROM tbl_students WHERE rfidnum='".$rfidnum."' AND id=".$st_id;
            $result = mysqli_query($conn,$sql);
            if(mysqli_num_rows($result) === 1){
                $student = mysqli_fetch_assoc($result);
                if($student['pay1'] === "0.00" && $student['pay2'] === "0.00" &&$student['pay3'] === "0.00"){
                    header('Location: index.php?id='.$ad_id.'&msg=fullpay');
                    exit();
                }else{
                    $amount = str_replace(',','',$_POST['amount']);
                    $pay1 = str_replace(',','',$student['pay1']);
                    $pay2 = str_replace(',','',$student['pay2']);
                    $pay3 = str_replace(',','',$student['pay3']);
                    $balance = str_replace(',','',$student['balance']);
                    $num_pay1 = (($pay1 + 0.01) - 0.01);
                    $num_pay2 = (($pay2 + 0.01) - 0.01);
                    $num_pay3 = (($pay3 + 0.01) - 0.01);
                    $num_amount = (($amount + 0.01) - 0.01);
                    $num_balance = (($balance + 0.01) - 0.01);
                    if($num_amount > $num_balance){
                        header('Location: index.php?id='.$ad_id.'&msg=low');
                        exit();
                    }else{
                        $count = 0;
                        if($num_amount > $num_pay1 && $num_pay1 > 0.00){
                            $string_pay1 = "0.00";
                            $num_amount = $num_amount - $num_pay1;
                        }else if($num_amount < $num_pay1 && $num_pay1 > 0.00){
                            $string_pay1 = $num_pay1 - $num_amount;
                            $num_amount = 0.00;
                        }else{
                        }
                        //echo $num_amount."<br>".$num_pay1."<br>".$string_pay1."<br>";
                        if($num_amount > $num_pay2 && $num_pay2 > 0.00){
                            $string_pay2 = "0.00";
                            $num_amount = $num_amount - $num_pay2;
                        }else if($num_amount < $num_pay2 && $num_pay2 > 0.00){
                            $string_pay2 = $num_pay2 - $num_amount;
                            $num_amount = 0.00;
                        }else{
                        }
                        //echo $num_amount."<br>".$num_pay2."<br>".$string_pay2."<br>";
                        if($num_amount > $num_pay3 && $num_pay3 > 0.00){
                            $num_amount = $num_amount - $num_pay3;
                        }else if($num_amount < $num_pay3 && $num_pay3 > 0.00){
                            $string_pay3 = $num_pay3 - $num_amount;
                            $num_amount = 0.00;
                        }else{
                        }

                        $string_balance = ($num_balance - $amount) + $num_amount;          
                        $final_pay1 = number_format($string_pay1,2,'.','');
                        $final_pay2 = number_format($string_pay2,2,'.','');
                        $final_pay3 = number_format($string_pay3,2,'.','');
                        $final_balance = number_format($string_balance,2,'.','');
                        $final_amount = number_format($amount,2,'.','');
                        $change = $num_amount;
                        $final_change = $change;
                        if($num_amount > 0.00){
                            $final_amount = number_format($amount - $num_amount,2,'.','');
                            $change = $amount - $final_amount;
                            $final_change = number_format($change,2,'.','');
                        }else{
                            $final_amount = number_format($amount,2,'.','');
                        }
                        $sql = "UPDATE tbl_students SET balance='".$final_balance."', pay1='".$final_pay1."', pay2='".$final_pay2."', pay3='".$final_pay3."' WHERE id=".$student['id'];
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            if($num_amount > 0.00){
                                // $sql2="INSERT INTO `tbl_transactions` (`id`, `activity`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `date`, `time`, `amount`) VALUES (NULL, NULL, 'Tuition Fee Payment via Teller Amount was PHP ".$amount.", Given Change is PHP ".$final_change."', '".$student['studnum']."', '".$student['rfidnum']."', '".$user."', 'TELLER', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."')";
                                $sql2 = "INSERT INTO `tbl_transactions` (`id`, `transnumber`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `direct`, `date`, `time`, `amount`, `cart`) VALUES (NULL, ".$count1.", 'Tuition Fee Payment via Teller Amount was PHP ".$amount.", Given Change is PHP ".$final_change."','".$student['studnum']."', '".$student['rfidnum']."', '".$user."', 'TELLER', 'TELLER', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."', NULL)";
                                $result2 = mysqli_query($conn,$sql2);
                                header('Location: index.php?id='.$ad_id.'&msg=success1');
                                exit();
                            }else{
                                // $sql2="INSERT INTO `tbl_transactions` (`id`, `activity`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `date`, `time`, `amount`) VALUES (NULL, NULL, 'Tuition Fee Payment via Teller', '".$student['studnum']."', '".$student['rfidnum']."', '".$user."', 'TELLER', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."')";
                                $sql2 = "INSERT INTO `tbl_transactions` (`id`, `transnumber`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `direct`, `date`, `time`, `amount`, `cart`) VALUES (NULL, ".$count1.", 'Tuition Fee Payment via Teller', '".$student['studnum']."', '".$student['rfidnum']."', '".$user."', 'TELLER', 'TELLER', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."', NULL)";
                                $result2 = mysqli_query($conn,$sql2);
                                header('Location: index.php?id='.$ad_id.'&msg=success1');
                                exit();
                            }
                        }else{
                            header('Location: index.php?id='.$ad_id.'&msg=error');
                            exit();
                        }
                    }
                }
            }else{
                header('Location: index.php?id='.$ad_id.'&msg=rfiderror');
                exit();
            }
        }     
    }else{
        
    }
?>