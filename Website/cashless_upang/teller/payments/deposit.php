<?php

include '../db_con.php';
    if(isset($_POST['insert'])){
        $ad_id = $_POST['id'];
        $st_id = $_POST['placing'];
        $rfidnum = $_POST['rfidnum'];
        $amount = str_replace(',','',$_POST['amount']);
        $count_sql = "SELECT * FROM tbl_transactions";
        $count_result = mysqli_query($conn,$count_sql);
        $count = mysqli_num_rows($count_result) + 1;
        if(empty($rfidnum)){
            header('Location: index.php?id='.$ad_id.'&msg=norfid');
            exit();
        }else{
            $sql="SELECT * FROM tbl_accounts WHERE id=".$ad_id;
            $result=mysqli_query($conn,$sql);
            if(mysqli_num_rows($result) === 1){
                $tenant=mysqli_fetch_assoc($result);
            }

            $sql = "SELECT * FROM tbl_students WHERE rfidnum='".$rfidnum."' AND id=".$st_id;
            $result = mysqli_query($conn,$sql);
            if(mysqli_num_rows($result) === 1){
                $row=mysqli_fetch_assoc($result);
                $balance = str_replace(',','',$row['balance']);
                if($balance === null){
                    $balance = "0.00";
                }
                $num_amount = ($amount + 0.01) - 0.01;
                $num_balance = ($balance + 0.01) - 0.01;
                $string_balance = ($balance + $amount);
                $final_balance = number_format($string_balance,2,'.','');
                $final_amount = number_format($amount,2,'.','');
                $sql="UPDATE tbl_students SET balance='".$final_balance."' WHERE id=".$row['id'];
                $result=mysqli_query($conn,$sql);
                if($result){
                    // $sql="INSERT INTO `tbl_transactions` (`id`, `activity`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `date`, `time`, `amount`) VALUES (NULL, NULL, 'Deposit via Teller', '".$row['studnum']."', '".$rfidnum."', '".$tenant['username']."', 'TELLER', CURRENT_DATE(), CURRENT_TIME(), '".$amount."')";
                    $sql = "INSERT INTO `tbl_transactions` (`id`, `transnumber`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `direct`, `date`, `time`, `amount`, `cart`) VALUES (NULL, ".$count.", 'Deposit via Teller', '".$row['studnum']."', '".$rfidnum."', '".$tenant['username']."', 'TELLER', 'TELLER', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."', NULL)";
                    $result=mysqli_query($conn,$sql);
                    header('Location: index.php?id='.$ad_id.'&msg=dep');
                    exit();
                }else{
                    header('Location: index.php?id='.$ad_id.'&msg=error');
                    exit();
                }
            }else{
                header('Location: index.php?id='.$ad_id.'&msg=rfiderror');
                exit();
            }
            
        }
        
    }
?>