<?php
    include "../db_con.php";
    $link_other=  "../regpay/?id=".$_GET['id'];
    $link_pay =  "../payments/?id=".$_GET['id'];
    $link_settings=  "../settings/?id=".$_GET['id'];
    $link_home=  "../?id=".$_GET['id'];
    $sql = "SELECT * FROM tbl_accounts WHERE id = ".$_GET['id'];
    $result = mysqli_query($conn, $sql); 
    if(mysqli_num_rows($result) === 1){
        $row = mysqli_fetch_assoc($result);
        $admin_lname = $row['lname'];
        $admin_fname = $row['fname'];
        $admin_mi = $row['mi'];
        // $balance = $row['balance'];
        // $pay1 = $row['pay1'];
        // $pay2 = $row['pay2'];
        // $pay3 = $row['pay3'];
}
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/solid.min.css" integrity="sha512-WTx8wN/JUnDrE4DodmdFBCsy3fTGbjs8aYW9VDbW8Irldl56eMj8qUvI3qoZZs9o3o8qFxfGcyQocUY0LYZqzQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/fontawesome.min.css" integrity="sha512-Rcr1oG0XvqZI1yv1HIg9LgZVDEhf2AHjv+9AuD1JXWGLzlkoKDVvE925qySLcEywpMAYA/rkg296MkvqBF07Yw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/datetime/1.1.1/css/dataTables.dateTime.min.css">
</head>
<body>
<div class="sidebar">
        <div class="logo-details">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span class="logo_name">CUP UPANG</span>
        
        </div>
        <i class='bx bx-menu' id="btn"></i>
        <ul class="nav-links">
            <li>
                <a href="<?php echo $link_home?>">
                    <i class='bx bxs-dashboard'></i>
                    <span class="link_name">Dashboard</span>
                </a>
                <!-- <span class="tooltip">Dashboard</span> -->
            </li>
            <li>
                <a href="<?php echo $link_pay?>">
                    <i class="fa-solid fa-money-bill"></i>
                    <span class="link_name">Payments</span>
                </a>
                <!-- <span class="tooltip">Students</span> -->
            </li>
            <li>
                <a href="<?php echo $link_other?>">
                    <i class='bx bx-basket'></i>
                    <span class="link_name">Register Payments</span>
                </a>
                <!-- <span class="tooltip">Tenants</span> -->
            </li>
            <li>
                <a href="">
                    <i class='bx bxs-user'></i>
                    <span class="link_name">Transactions</span>
                </a>
                <!-- <span class="tooltip">User Accounts</span> -->
            </li>
            <li>
                <a href="<?php echo $link_settings?>">
                    <i class='bx bx-cog'></i>
                    <span class="link_name">Settings</span>
                </a>
                <!-- <span class="tooltip">Settings</span> -->
            </li>
        </ul>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <i class='bx bxs-user-circle' ></i>
                    <div class="name_job">
                        <div class="name"><?php echo $admin_fname;?></div>
                        <div class="job">Teller</div>
                </div>
                </div>
                <i class='bx bx-log-out' id=log_out></i>
            </div>
            
        </div>
        
    </div>
    <script>
        let btn = document.querySelector("#btn");
        let sidebar = document.querySelector(".sidebar");
        let logout = document.querySelector("#log_out");
        btn.onclick = function(){
            sidebar.classList.toggle("active");
        }
        logout.onclick = function(){
            var r = confirm("Are you sure you want to logout???");
            if(r==true){
                window.location.href = "../../";
            }
        }
    </script>
    <div class="home-section">
        
        <div class="container">
            
            <div style="width:100%; text-align: center;"><h2>My Transactions</h2></div>
            <div class="jumbotron">
            <table cellspacing="5" cellpadding="5">
                <tbody>
                    <tr>
                        <td>Minimum date:</td>
                        <td><input type="text" id="min" name="min"></td>
                    </tr>
                    <tr>
                        <td>Maximum date:</td>
                        <td><input type="text" id="max" name="max"></td>
                    </tr>
                </tbody>
            </table>
            <table id="example" class="display nowrap table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <td>Transaction Number</td>
                        <td>Description</td>
                        <td colspan=2 style="text-align: center;">Amount</td>
                        <td style="text-align: center;">Date</td>
                        <td style="text-align: center;">Time</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <?php
                        $sql = "SELECT * FROM tbl_transactions WHERE transid='".$row['username']."' ORDER BY id DESC";
                        $result = mysqli_query($conn, $sql); 
                        if($result){
                            while($row=mysqli_fetch_assoc($result)){
                                ?>
                                <tr>
                                    <td><?php echo $row['transnumber']?></td>
                                    <td><?php echo $row['desc'];?></td>
                                    <td><i class="fa-solid fa-peso-sign"></i></td>
                                    <td style="text-align: right;"><?php echo number_format((float)$row['amount'], 2, '.', ',')?></td>
                                    <td style="text-align: center;"><?php echo $row['date']?></td>
                                    <td style="text-align: center;"><?php echo $row['time']?></td>
                                </tr>
                                <?php
                                }
                            }else{
                            }
                            ?>
                    </tr>
                <tfoot>
                    <tr>
                        <td>Transaction Number</td>
                        <td>Description</td>
                        <td colspan=2 style="text-align: center;">Amount</td>
                        <td style="text-align: center;">Date</td>
                        <td style="text-align: center;">Time</td>
                    </tr>
                </tfoot>
            </table>
            
            
            
            
            <!-- <table id="myTable" class="table table-striped" style="width: 100%; height: 3px; ">
                <thead>
                    <tr>
                        <td>Description</td>
                        <td colspan=2 style="text-align: center;">Amount</td>
                        <td style="text-align: center;">Date</td>
                        <td style="text-align: center;">Time</td>
                    </tr>
                </thead>
                <tbody>
                <?php
                        // $sql = "SELECT * FROM tbl_transactions WHERE transid='".$row['username']."' ORDER BY id DESC";
                        // $result = mysqli_query($conn, $sql); 
                        // if($result){
                        //     while($row=mysqli_fetch_assoc($result)){
                                ?>
                                <tr>
                                    <td><?php //echo $row['desc'];?></td>
                                    <td><i class="fa-solid fa-peso-sign"></i></td>
                                    <td style="text-align: right;"><?php //echo number_format((float)$row['amount'], 2, '.', ',')?></td>
                                    <td style="text-align: center;"><?php //echo $row['date']?></td>
                                    <td style="text-align: center;"><?php //echo $row['time']?></td>
                                </tr>
                                <?php
                            //     }
                            // }else{
                            // }
                            ?>
                </tbody>
            </table> -->
            </div>
        </div>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <!-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script> -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
        <script>
            var minDate, maxDate;
 
 // Custom filtering function which will search data in column four between two values
            $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    var min = minDate.val();
                    var max = maxDate.val();
                    var date = new Date( data[4] );
            
                    if (
                        ( min === null && max === null ) ||
                        ( min === null && date <= max ) ||
                        ( min <= date   && max === null ) ||
                        ( min <= date   && date <= max )
                    ) {
                        return true;
                    }
                    return false;
                }
            );
  
            $(document).ready(function() {
                // Create date inputs
                minDate = new DateTime($('#min'), {
                    format: 'MMMM Do YYYY'
                });
                maxDate = new DateTime($('#max'), {
                    format: 'MMMM Do YYYY'
                });
            
                // DataTables initialisation
                var table = $('#example').DataTable();
            
                // Refilter the table
                $('#min, #max').on('change', function () {
                    table.draw();
                });
            });
        </script>
    </div>
</body>
</html>
