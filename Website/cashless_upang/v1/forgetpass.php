<?php

    require_once '../DBOperations.php';
    require 'database.php';
    $response = array();

    if($_SERVER['REQUEST_METHOD']=='POST'){

        if(isset($_POST['user'])){
            $db = new DBOperation();

            if($db->userLogin($_POST['user'])){
                $ticket = $db->getuserByusername($_POST['user']);
                if($ticket['pword'] != $_POST['opass']){
                    echo 'Wrong Old Password';
                }else{
                    $pdo = Database::connect();
                    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    		    $sql = "UPDATE tbl_students set pword = ? WHERE studnum = ?";
	    		    $q = $pdo->prepare($sql);
	    	        $q->execute(array($_POST['npass'],$_POST['user']));
                    $pdo = Database::connect();
                    echo 'Password Reset';
                }
            }else{
                echo 'No User Found';
            }
        }else{
            echo 'Invalid User';
        }
    }else{
        echo 'Server Request Error';
    }
    
?>