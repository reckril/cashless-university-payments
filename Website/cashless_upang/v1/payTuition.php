<?php
    include '../db_con.php';
    $studnum = $_POST['studnum'];
    $sql = "SELECT * FROM tbl_students WHERE studnum='".$studnum."'";
    $result = mysqli_query($conn,$sql);
    $count_sql = "SELECT * FROM tbl_transactions";
    $count_result = mysqli_query($conn,$count_sql);
    $count = mysqli_num_rows($count_result) + 1;
    if(mysqli_num_rows($result)===1){
        $student = mysqli_fetch_assoc($result);
        if($student["pay1"] === "0.00" && $student["pay2"] === "0.00" && $student["pay3"] === "0.00"){
            echo 'paid';
        }else{
            $string_pay1 = $student['pay1'];
            $string_pay2 = $student['pay2'];
            $string_pay3 = $student['pay3'];
            $amount = str_replace(',','',$_POST['amount']);
            $balance = str_replace(',','',$student['balance']);
            $pay1 = str_replace(',','',$student['pay1']);
            $pay2 = str_replace(',','',$student['pay2']);
            $pay3 = str_replace(',','',$student['pay3']);
            $num_amount = ($amount + 0.01) - 0.01;
            $num_balance = ($balance + 0.01) - 0.01;
            $num_pay1 = ($pay1 + 0.01) - 0.01;
            $num_pay2 = ($pay2 + 0.01) - 0.01;
            $num_pay3 = ($pay3 + 0.01) - 0.01;
            if($num_balance >= $num_amount){
                if($num_amount > $num_pay1 && $num_pay1 > 0.00){
                    $string_pay1 = "0.00";
                    $num_amount = $num_amount - $num_pay1;
            }else if($num_amount < $num_pay1 && $num_pay1 > 0.00){
                $string_pay1 = $num_pay1 - $num_amount;
                $num_amount = 0.00;
            }else{
            }

            if($num_amount > $num_pay2 && $num_pay2 > 0.00){
                $string_pay2 = "0.00";
                $num_amount = $num_amount - $num_pay2;
            }else if($num_amount < $num_pay2 && $num_pay2 > 0.00){
                $string_pay2 = $num_pay2 - $num_amount;
                $num_amount = 0.00;
            }else{
            }

            if($num_amount > $num_pay3 && $num_pay3 > 0.00){
                $string_pay3 = "0.00";
                $num_amount = $num_amount - $num_pay3;
            }else if($num_amount < $num_pay3 && $num_pay3 > 0.00){
                $string_pay3 = $num_pay3 - $num_amount;
                $num_amount = 0.00;
            }else{
            }

            $string_balance = ($num_balance - $amount) + $num_amount;          
            // echo "Amount: ".$amount."<br> Float of Amount after deduction: ".$num_amount."<br> Tuition Fee 1st Payment:".$pay1."<br> Tuition Fee 1st Payment Paid:".$string_pay1."<br> Tuition Fee 2nd Payment:".$pay2."<br> Tuition Fee 2nd Payment Paid:".$string_pay2."<br> Tuition Fee 3rd Payment:".$pay3."<br> Tuition Fee 3rd Payment Paid:".$string_pay3."<br> Balance before:".$num_balance."<br> Balance after:".$string_balance;
            $final_pay1 = number_format($string_pay1,2,'.','');
            $final_pay2 = number_format($string_pay2,2,'.','');
            $final_pay3 = number_format($string_pay3,2,'.','');
            $final_balance = number_format($string_balance,2,'.','');
            $final_amount = number_format($amount,2,'.','');
            $change = $num_amount;
            $final_change = $change;
            if($num_amount > 0.00){
                $final_amount = number_format($amount - $num_amount,2,'.','');
                $change = $amount - $final_amount;
                $final_change = number_format($change,2,'.','');
            }else{
                $final_amount = number_format($amount,2,'.','');
            }
            $sql = "UPDATE tbl_students SET balance='".$final_balance."', pay1='".$final_pay1."', pay2='".$final_pay2."', pay3='".$final_pay3."' WHERE id=".$student['id'];
            $result = mysqli_query($conn,$sql);
            if($result){
                if($num_amount > 0.00){
                    // $sql2="INSERT INTO `tbl_transactions` (`id`, `activity`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `date`, `time`, `amount`) VALUES (NULL, NULL, 'Tuition Fee Payment using Mobile Amount was PHP ".$amount.", Given Change is PHP ".$final_change."', '".$student['studnum']."', '".$student['rfidnum']."', 'STUDENT', 'MOBILE', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."')";
                    $sql2="INSERT INTO `tbl_transactions` (`id`, `transnumber`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `direct`, `date`, `time`, `amount`, `cart`) VALUES (NULL, ".$count.", 'Tuition Fee Payment using Mobile Amount was PHP ".$amount.", Given Change is PHP ".$final_change."', '".$student['studnum']."', '".$student['rfidnum']."', 'STUDENT', 'MOBILE', 'TELLER', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."', NULL)";
                    $result2 = mysqli_query($conn,$sql2);
                    echo 'Complete';
                }else{
                    // $sql2="INSERT INTO `tbl_transactions` (`id`, `activity`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `date`, `time`, `amount`) VALUES (NULL, NULL, 'Tuition Fee Payment using Mobile', '".$student['studnum']."', '".$student['rfidnum']."', 'STUDENT', 'MOBILE', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."')";
                    $sql2="INSERT INTO `tbl_transactions` (`id`, `transnumber`, `desc`, `studnum`, `rfid`, `transid`, `paymethod`, `direct`, `date`, `time`, `amount`, `cart`) VALUES (NULL, ".$count.", 'Tuition Fee Payment using Mobile', '".$student['studnum']."', '".$student['rfidnum']."', 'STUDENT', 'MOBILE', 'TELLER', CURRENT_DATE(), CURRENT_TIME(), '".$final_amount."', NULL)";
                    $result2 = mysqli_query($conn,$sql2);
                    echo 'Complete';
                }
            }else{
                echo 'error';
            }
        }else{
            echo 'balance_short';
        }
        }
    }else{
        echo 'error';
    }
?>