<?php

    require_once '../DBOperations.php';
    require 'database.php';
    $response = array();

    if($_SERVER['REQUEST_METHOD']=='POST'){

        if(isset($_POST['user'])){
            $db = new DBOperation();

            if($db->userLogin($_POST['user'])){
                $ticket = $db->getuserByusername($_POST['user']);
                if($ticket['email'] != $_POST['email']){
                    echo 'Wrong Email';
                }else{
                    echo 'Email Found';
                }
            }else{
                echo 'No User Found';
            }
        }else{
            echo 'Invalid User';
        }
    }else{
        echo 'Server Request Error';
    }
    
?>