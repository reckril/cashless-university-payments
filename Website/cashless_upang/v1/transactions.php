<?php
    require_once "../DBOperations.php";

    $response = array();

    $db = new DBOperation;

    $out = $db->showTransaction();

    while($result = $out->fetch_assoc()){

        if($result['Month'] == 1){
            $month = "JAN";
        }else if($result['Month'] == 2){
            $month = "FEB";
        }else if($result['Month'] == 3){
            $month = "MAR";
        }else if($result['Month'] == 4){
            $month = "APR";
        }else if($result['Month'] == 5){
            $month = "MAY";
        }else if($result['Month'] == 6){
            $month = "JUN";
        }else if($result['Month'] == 7){
            $month = "JUL";
        }else if($result['Month'] == 8){
            $month = "AUG";
        }else if($result['Month'] == 9){
            $month = "SEP";
        }else if($result['Month'] == 10){
            $month = "OCT";
        }else if($result['Month'] == 11){
            $month = "NOV";
        }else if($result['Month'] == 12){
            $month = "DEC";
        }

        if($result['Hour'] <= 11 && $result['Hour'] != 0){
                            if($result['Minute'] < 10){
                                $time = $result['Hour'].':0'.$result['Minute'].' A.M.';
                            }else{
                                $time = $result['Hour'].':'.$result['Minute'].' A.M.';
                            }
                        }else if($result['Hour'] == 0){
                            if($result['Minute'] < 10){
                                $time = '12:0'.$result['Minute'].' A.M.';
                            }else{
                                $time = '12:'.$result['Minute'].' A.M.';
                            }
                        }else if($result['Hour'] == 12){
                            if($result['Minute'] < 10){
                                $time = '12:0'.$result['Minute'].' P.M.';
                            }else{
                                $time = '12:'.$result['Minute'].' P.M.';
                            }
                        }else{
                            $hourconv = $result['Hour'] - 12;
                            if($result['Minute'] < 10){
                                $time = $time = $hourconv.':0'.$result['Minute'].' P.M.';;
                            }else{
                                $time = $time = $hourconv.':'.$result['Minute'].' P.M.';;
                        }
                    }


        $temp = array();
        $temp['id'] = $result['id'];
        $temp['desc'] = $result['desc'];
        $temp['studnum'] = $result['studnum'];
        $temp['datetime'] = $month.' '.$result['Day'].', '.$result['Year']." AT ". $time;
        $temp['amount'] = 'PHP '.$result['amount'];
        array_push($response,$temp);
    }

    echo json_encode($response);
?>