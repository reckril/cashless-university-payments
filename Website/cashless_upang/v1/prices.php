<?php
    require_once "../DBOperations.php";

    $response['output'] = array();

    $db = new DBOperation;

    $out = $db->showPrice();

    while($result = $out->fetch_assoc()){
        $temp = array();
        $temp['id'] = $result['id'];
        
        array_push($response['output'],$temp);
    }

    echo json_encode($response);
?>