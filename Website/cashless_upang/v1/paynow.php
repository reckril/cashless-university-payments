<?php

    require_once '../DBOperations.php';
    require 'database.php';
    $response = array();

    if($_SERVER['REQUEST_METHOD']=='POST'){

        if(isset($_POST['user'])){
            $db = new DBOperation();

            if($db->userLogin($_POST['user'])){   
            $ticket = $db->getuserByusername($_POST['user']);
            if(floatval($ticket[$_POST["amount"]]) > floatval($ticket["balance"])){
                echo "Insufficient Balance";
            }else{
                $amount = str_replace(",","",$ticket[$_POST["amount"]]);
                $amount2 = floatval($amount);
                $bal = str_replace(",","",$ticket["balance"]);
                $bal2 = floatval($bal);
                $total = number_format($bal2 - $amount2,2,".",",");
                $string_total = str_replace("float(","",strval($total));
                $pdo = Database::connect();
	    		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    		$sql = "UPDATE tbl_students set balance = ?, ".$_POST["amount"]." = ? WHERE studnum = ?";
	    		$q = $pdo->prepare($sql);
	    	    $q->execute(array($string_total,"0.00",$_POST['user']));
                $transcode = "tum";
                $paymethod = "MOBILE";
                $pdo = Database::connect();
	            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	            $sql = "INSERT INTO tbl_transactions(activity, studnum, rfid, paymethod, date, time, amount) values(?, ?, ?, ?, CURRENT_DATE, CURRENT_TIME, ?)";
	            $q = $pdo->prepare($sql);
	            $q->execute(array($transcode,$ticket['studnum'],$ticket['rfidnum'],$paymethod,$amount2));
	            Database::disconnect();
                echo "Tuition Fee Paid";
            } 
                }
            else{
                echo "";
            }
        }else{
            echo "";
        }
    }else{
        echo "";
    }
?>