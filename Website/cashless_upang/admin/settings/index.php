<?php
    include "../db_con.php";
    $link_home = "../?id=".$_GET['id'];
    $link_teller =  "../user/?id=".$_GET['id'];
    $link_tenants=  "../tenants/?id=".$_GET['id'];
    $link_student=  "../student/?id=".$_GET['id'];
    $sql = "SELECT * FROM tbl_accounts WHERE id = ".$_GET['id'];
    $result = mysqli_query($conn, $sql); 
    if(mysqli_num_rows($result) === 1){
        $row = mysqli_fetch_assoc($result);
            $admin_id = $row['id'];
            $admin_lname = $row['lname'];
            $admin_fname = $row['fname'];
            $admin_mi = $row['mi'];
    }
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<div class="sidebar">
        <div class="logo-details">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span class="logo_name">CUP UPANG</span>
        
        </div>
        <i class='bx bx-menu' id="btn"></i>
        <ul class="nav-links">
            <li>
                <a href="<?php echo $link_home?>">
                    <i class='bx bxs-dashboard'></i>
                    <span class="link_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_student?>">
                    <i class='bx bx-book-reader'></i>
                    <span class="link_name">Students</span>
                </a>
                <span class="tooltip">Students</span>
            </li>
            <li>
                <a href="<?php echo $link_tenants?>">
                    <i class='bx bx-basket'></i>
                    <span class="link_name">Tenants</span>
                </a>
                <span class="tooltip">Tenants</span>
            </li>
            <li>
                <a href="<?php echo $link_teller?>">
                    <i class='bx bxs-user'></i>
                    <span class="link_name">User Accounts</span>
                </a>
                <span class="tooltip">User Accounts</span>
            </li>
            <li>
                <a href="">
                    <i class='bx bx-cog'></i>
                    <span class="link_name">Settings</span>
                </a>
                <span class="tooltip">Settings</span>
            </li>
        </ul>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <i class='bx bxs-user-circle' ></i>
                    <div class="name_job">
                        <div class="name"><?php echo $admin_lname.", ".$admin_fname." ".$admin_mi.".";?></div>
                        <div class="job">Administrator</div>
                </div>
                </div>
                <i class='bx bx-log-out' id=log_out></i>
            </div>
            
        </div>
        
    </div>
    <script>
        let btn = document.querySelector("#btn");
        let sidebar = document.querySelector(".sidebar");
        let logout = document.querySelector("#log_out");
        btn.onclick = function(){
            sidebar.classList.toggle("active");
        }
        logout.onclick = function(){
            var r = confirm("Are you sure you want to logout???");
            if(r==true){
                window.location.href = "../../";
            }
        }
    </script>
    <div class="home-section">
        <div class="container">
        <?php 
                if(isset($_GET['msg'])){ 
                    if($_GET['msg'] === "success1"){
                        ?><div class="change"><p class="success">Password has successfully changed!</p></div><?php
                    }else if($_GET['msg'] === "user"){
                        ?><div class="error"><p>The Username you are trying to register has already been taken!</p></div><?php
                    }else if($_GET['msg'] === "nochange"){
                        ?><div class="error"><p>Update Failed: No Changes</p></div><?php
                    }else if($_GET['msg'] === "error"){
                        ?><p class="error">System has Failed to do action!</p><?php
                    }else{
                        ?><div class="success"><p><?php echo $_GET['msg']?></p></div><?php
                    }
                }
        ?>
            <div class="container_card">
            <div><h4>Change Password</h4></div>
            <div>
                <form action="changePass.php" method="POST">
                    <input type="hidden" name="id" value="<?php echo $admin_id;?>">
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">New Password</label>
                        <input type="password" class="form-control" id="exampleFormControlInput1" name="newpass" required="required">
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">New Repeat Password</label>
                        <input type="password" class="form-control" id="exampleFormControlInput1" name="newrpass" required="required">
                    </div>
                    <div><button type="submit" class="btn btn-primary" name="insert" style="width:60%;">Change Password</button></div>
                </form>
            </div>
            </div>
            <div class="container_card">
            <div><h4>Edit Admin Details</h4></div>
            <div class="form_edit">
            <form action="changeUser.php" method="POST">
                <input type="hidden" name="id" value="<?php echo $_GET['id']?>">
                <input type="hidden" name="placing" id="update_id">
                    <label for="exampleFormControlInput1" class="form-label">Name</label>
                    <div class="row g-3">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Last" aria-label="Last name" name="lname" required="required" value="<?php echo $admin_lname?>">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="First" aria-label="First name" name="fname" required="required" value="<?php echo $admin_fname?>">
                    </div>
                    <div class="col">
                    <input type="text" class="form-control" placeholder="Middle Initial" aria-label="First name" name="mi" required="required" maxlength="2" value="<?php echo $admin_mi?>">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Username</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="user" required="required"value="<?php echo $row['username']?>">
                </div><br/>
                <div><button type="submit" class="btn btn-primary" name="insert" style="width:60%;">Change Account Details</button></div>
            </form>
        </div>
        </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>