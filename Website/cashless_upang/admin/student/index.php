<?php
	$Write="<?php $" . "UIDresult=''; " . "echo $" . "UIDresult;" . " ?>";
	file_put_contents('UIDContainer.php',$Write);
?>
<?php
    include "../db_con.php";
    $link_home = "../?id=".$_GET['id'];
    $link_teller =  "../user/?id=".$_GET['id'];
    $link_tenants=  "../tenants/?id=".$_GET['id'];
    $link_settings=  "../settings/?id=".$_GET['id'];
    $sql = "SELECT * FROM tbl_accounts WHERE id = ".$_GET['id'];
    $result = mysqli_query($conn, $sql); 
    if(mysqli_num_rows($result) === 1){
        $row = mysqli_fetch_assoc($result);
            $admin_lname = $row['lname'];
            $admin_fname = $row['fname'];
            $admin_mi = $row['mi'];
    }
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/solid.min.css" integrity="sha512-WTx8wN/JUnDrE4DodmdFBCsy3fTGbjs8aYW9VDbW8Irldl56eMj8qUvI3qoZZs9o3o8qFxfGcyQocUY0LYZqzQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/fontawesome.min.css" integrity="sha512-Rcr1oG0XvqZI1yv1HIg9LgZVDEhf2AHjv+9AuD1JXWGLzlkoKDVvE925qySLcEywpMAYA/rkg296MkvqBF07Yw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css"> -->
</head>
<body>
<div class="sidebar">
        <div class="logo-details">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span class="logo_name">CUP UPANG</span>
        
        </div>
        <i class='bx bx-menu' id="btn"></i>
        <ul class="nav-links">
            <li>
                <a href="<?php echo $link_home?>">
                    <i class='bx bxs-dashboard'></i>
                    <span class="link_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="">
                    <i class='bx bx-book-reader'></i>
                    <span class="link_name">Students</span>
                </a>
                <span class="tooltip">Students</span>
            </li>
            <li>
                <a href="<?php echo $link_tenants?>">
                    <i class='bx bx-basket'></i>
                    <span class="link_name">Tenants</span>
                </a>
                <span class="tooltip">Tenants</span>
            </li>
            <li>
                <a href="<?php echo $link_teller?>">
                    <i class='bx bxs-user'></i>
                    <span class="link_name">User Accounts</span>
                </a>
                <span class="tooltip">User Accounts</span>
            </li>
            <li>
                <a href="<?php echo $link_settings?>">
                    <i class='bx bx-cog'></i>
                    <span class="link_name">Settings</span>
                </a>
                <span class="tooltip">Settings</span>
            </li>
        </ul>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <i class='bx bxs-user-circle' ></i>
                    <div class="name_job">
                        <div class="name"><?php echo $admin_lname.", ".$admin_fname." ".$admin_mi.".";?></div>
                        <div class="job">Administrator</div>
                </div>
                </div>
                <i class='bx bx-log-out' id=log_out></i>
            </div>
            
        </div>
        
    </div>
    <script>
        let btn = document.querySelector("#btn");
        let sidebar = document.querySelector(".sidebar");
        let logout = document.querySelector("#log_out");
        btn.onclick = function(){
            sidebar.classList.toggle("active");
        }
        logout.onclick = function(){
            var r = confirm("Are you sure you want to logout???");
            if(r==true){
                window.location.href = "../../";
            }
        }
    </script>
    <div class="home-section">
        
        <div class="container">
            <!-- Add Modal -->
            <div class="modal fade" id="student_addmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Register A Student</h5>
                        </div>
                        <div class="modal-body">
                            <form action="insertData.php" method="POST">
                                <input type="hidden" name="id" value="<?php echo $_GET['id']?>">
                                <label for="exampleFormControlInput1" class="form-label">Name</label>
                                <div class="row g-3">
                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Last" aria-label="Last name" name="lname" required="required">
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control" placeholder="First" aria-label="First name" name="fname" required="required">
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Middle Initial" aria-label="First name" name="mi" required="required" maxlength="2">
                                </div>
                                
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Student Number</label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="ex. 01-2345-67890" name="studnum" required="required">
                        </div>
                        <label for="input-group-text" class="form-label">Total Tuition Fee</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text"><i class="fa-solid fa-peso-sign"></i></span>
                                    <input type="text" id="tuition" class="form-control" aria-label="" placeholder="" name='total' required="required" onkeypress="return isNumberKey(event)">
                                <!-- <span class="input-group-text">.00</span> -->
                            </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" name="email" required="required">
                        </div> 
                        </div>
                        <div class="mb-3" style="padding: 1px 20px;">
                            <label for="exampleFormControlTextarea1" class="form-label">CUP NUMBER</label>
                            <textarea class="form-control getUID" id="exampleFormControlTextarea1" rows="1" cols="1" required="required" name="rfidnum" style="resize: none;"></textarea>
                        </div>

                        <!-- <div class="row g-3">
                            <div class="col">
                                <input type="text" class="form-control" id="pay1" placeholder="Last" aria-label="Last name" name="pay1" required="required">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" id="pay2" placeholder="First" aria-label="First name" name="pay2" required="required">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" id="pay3" placeholder="Middle Initial" aria-label="First name" name="pay3" required="required">
                            </div>
                        </div> -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" name="insert">Add Student</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <SCRIPT language=Javascript>
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
        //-->
        </SCRIPT>
            <!-- Edit Modal -->
            
            <div class="modal fade" id="editmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Register A Student</h5>
                        </div>
                        <div class="modal-body">
                        <form action="updateData.php" method="POST">
                            <input type="hidden" name="id" value="<?php echo $_GET['id']?>">
                            <input type="hidden" name="placing" id="update_id">
                            <label for="exampleFormControlInput1" class="form-label">Name</label>
                            <div class="row g-3">
                            <div class="col">
                                <input type="text" class="form-control" id="update_lname" placeholder="Last" aria-label="Last name" name="lname" required="required">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" id="update_fname" placeholder="First" aria-label="First name" name="fname" required="required">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" id="update_mi" placeholder="Middle Initial" aria-label="First name" name="mi" required="required" maxlength="2">
                            </div>
                            <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Student Number</label>
                            <input type="text" class="form-control update_studnum" id="exampleFormControlInput1" placeholder="ex. 01-2345-67890" name="studnum" required="required">
                        </div>
                        <label for="input-group-text" class="form-label">Total Tuition Fee</label>
                            <div class="input-group mb-3">
                                <span class="input-group-text"><i class="fa-solid fa-peso-sign"></i></span>
                                    <input type="text" id="tuition" class="form-control tuition" aria-label="" placeholder="" name='total' required="required" onkeypress="return isNumberKey(event)">
                                <!-- <span class="input-group-text">.00</span> -->
                            </div>
                        <!-- <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Password</label>
                            <input type="password" class="form-control update_pword" id="exampleFormControlInput1" placeholder="ex. 01-2345-67890" name="pword" required="required">
                        </div>  -->
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Email address</label>
                            <input type="text" class="form-control update_email" id="exampleFormControlInput1" placeholder="name@example.com" name="email" required="required">
                        </div> 
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Current CUP Number</label>
                            <input type="text" class="form-control update_rfidnum" id="exampleFormControlInput1" placeholder="name@example.com" name="curr_rfidnum" required="required" readonly=readonly>
                        </div> 
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlTextarea1" class="form-label">New CUP NUMBER</label>
                            <textarea class="form-control getUID" id="exampleFormControlTextarea1" rows="1" cols="1" name="rfidnum" style="resize: none;"></textarea>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="flexCheckDefault" name="defpass">
                            <label class="form-check-label" for="flexCheckDefault">
                                Use the Default Password
                            </label>
                        </div>
                        <!-- <div class="row g-3">
                            <div class="col">
                                <input type="text" class="form-control update_pay1" placeholder="Last" aria-label="Last name" name="pay1" required="required">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control update_pay2" placeholder="First" aria-label="First name" name="pay2" required="required">
                            </div>
                            <div class="col">
                                <input type="text" class="form-control update_pay3" placeholder="Middle Initial" aria-label="First name" name="pay3" required="required" maxlength="2">
                            </div>
                        </div> -->
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" name="insert">Update</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- Delete Modal -->
            <div class="modal fade" id="delmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form action="deleteData.php" method="POST">
                        <div class="modal-body">
                            Are You Sure You Want to Delete This Record?
                                <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                                <input type="hidden" name="placing" id="delete_id">
                        </div>       
                        <div class="modal-footer">
                                <button type="submit" class="btn btn-success" name="insert">Yes</button>
                                <button type="button" class="btn btn-danger delbtn" data-bs-dismiss="modal">No</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>



            <div class="jumbotron">
                <?php 
                if(isset($_GET['msg'])){ 
                    if($_GET['msg'] === "addsuccess"){
                        ?><div class="success"><p class="sucess">Record has Successfully Added!</p></div><?php
                    }else if($_GET['msg'] === "studnum1error"){
                        ?><div class="error"><p>The Student Number you are trying to register has already been taken!</p></div><?php
                    }else if($_GET['msg'] === "rfiderror"){
                        ?><div class="error"><p>This CUP Card you are trying to register has already been registered to another user!<br/>Incase of Loss please give this back to the University.</p></div><?php
                    }else if($_GET['msg'] === "studnum2error"){
                        ?><div class="error"><p>The Student Number has already been taken please check what you are editing!</p></div><?php
                    }else if($_GET['msg'] === "update"){
                        ?><div class="success"><p>We have updated everything successfully!</p></div><?php
                    }else if($_GET['msg'] === "delete"){
                        ?><div class="success"><p>We have updated everything successfully!</p></div><?php
                    }else if($_GET['msg'] === "nochange"){
                        ?><div class="error"><p>Update Failed: No Changes</p></div><?php
                    }else if($_GET['msg'] === "dupli"){
                        ?><div class="error"><p>Duplication Error! Please Delete the other Duplicated Field</p></div><?php
                    }else if($_GET['msg'] === "error"){
                        ?><p class="error">System has Failed to do action!</p><?php
                    }else if($_GET['msg'] === "delerror"){
                        ?><p class="error">System has Failed to delete this record!</p><?php
                    }else{
                        ?><div class="success"><p><?php echo $_GET['msg']?></p></div><?php
                    }
                }?>
            <button type="button" class="btn btn-primary" id="clearswipe" data-bs-toggle="modal" data-bs-target="#student_addmodal">
                Add Student
            </button>

            <div class="input-group mb-3" style="margin-top: 5px;">
                <span class="input-group-text" id="basic-addon1"><i class='bx bx-search-alt-2'></i> Search Student Number:</span>
                <input type="text" id="myInput" class="form-control" onkeyup="myFunction()" placeholder="Caution:Case Sensitive" aria-label="Username" aria-describedby="basic-addon1">
            </div>
            <table id="myTable" class="table table-striped" style="width: 100%; height: 3px;">
                <thead>
                    <tr>
                        <td>Student Number</td>
                        <td>Name</td>
                        <td style="text-align: center;" colspan=2>Tuition Fee</td>
                        <td style="text-align: center;">Action</td>
                    </tr>
                </thead>
                <tbody>
                <?php
                        $sql = "SELECT * FROM tbl_students ORDER BY studnum ASC";
                        $result = mysqli_query($conn, $sql); 
                        if($result){
                            while($row=mysqli_fetch_assoc($result)){
                                
                                $total = (((str_replace(',','',$row['pay1']) + 0.01) - 0.01)+((str_replace(',','',$row['pay2']) + 0.01) - 0.01)+((str_replace(',','',$row['pay3']) + 0.01) - 0.01));
                                $total1 = number_format($total,2,'.','');
                                ?>
                                <tr>
                                        <td hidden><?php echo $row['id'];?></td>
                                        <td hidden><?php echo $row['studnum'];?></td>
                                        <td hidden><?php echo $row['pword'];?></td>
                                        <td hidden><?php echo $row['lname'];?></td>
                                        <td hidden><?php echo $row['fname'];?></td>
                                        <td hidden><?php echo $row['mi'];?></td>
                                        <td hidden><?php echo $row['email'];?></td>
                                        <td hidden><?php echo $row['rfidnum'];?></td>
                                        <td hidden><?php echo $total1;?></td>
                                        <td><?php echo $row['studnum'];?></td>
                                        <td><?php echo $row['lname'].", ".$row['fname']." ".$row['mi'].".";?></td>
                                        <td></td>
                                        <td style='text-align: right;'><?php echo $total1;?></td>
                                        <td style="text-align: center;">
                                            <button type="button" class="btn btn-success editbtn"><i class='bx bxs-edit-alt'></i></button>
                                            <button type="button" class="btn btn-danger deletebtn"><i class='bx bxs-trash' ></i></button>
                                        </td>
                                </tr>
                                <?php
                                }
                            }else{
                            }
                            ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
                $('.editbtn').on('click', function(){
                    $('#editmodal').modal('show');
                        $tr = $(this).closest('tr');

                        var data = $tr.children("td").map(function() {
                            return $(this).text();
                        }).get();
                    console.log(data)
                    var id = data[0].trim();
                    var studnum = data[1].trim();
                    // var pword = data[2].trim();
                    var lname = data[3].trim();
                    var fname = data[4].trim();
                    var mi = data[5].trim();
                    var email = data[6].trim();
                    var rfid = data[7].trim();
                    let tuition = data[8].trim();
                    $('#update_id').val(id);
                    $('.update_studnum').val(studnum);
                    // $('.update_pword').val(pword);
                    $('#update_lname').val(lname);
                    $('#update_fname').val(fname);
                    $('#update_mi').val(mi);
                    $('.update_email').val(email);
                    $('.update_rfidnum').val(rfid);
                    $('.tuition').val(tuition);
                });
            });
    </script>

    <script>
        $(document).ready(function(){
            $('.deletebtn').on('click', function(){
                    $('#delmodal').modal('show');
                    $tr = $(this).closest('tr');

                        var data = $tr.children("td").map(function() {
                            return $(this).text();
                        }).get();
                    console.log(data)
                    var id = data[0].trim();
                    $('#delete_id').val(id);
            });
        });
    </script>

<script>
    function myFunction() {
        // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value;
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[9];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
                }
            }
        }
    }
</script>

    <script>
			$(document).ready(function(){
                // $(".getUID").change(function(){
                //     if(this.change){
                //         if(this.val() == ""){
                //             $(".getUID").load("UIDContainer.php");
				//             setInterval(function() {
				// 	        $(".getUID").load("UIDContainer.php");
				//             }, 500);
                //         }else{
                //         }
                //     }else{
                //         $(".getUID").load("UIDContainer.php");
				//         setInterval(function() {
				// 	    $(".getUID").load("UIDContainer.php");
				//         }, 500);
                //     }
                // });
				 $(".getUID").load("UIDContainer.php");
				setInterval(function() {
					$(".getUID").load("UIDContainer.php");
				}, 500);
			});
            let addStudentBtn = document.querySelector("#clearswipe");

            addStudentBtn.onclick() = function (){
                <?php
	                $Write="<?php $" . "UIDresult=''; " . "echo $" . "UIDresult;" . " ?>";
	                file_put_contents('UIDContainer.php',$Write);
                ?>
            }
		</script>

</body>
</html>