<?php
    include "../db_con.php";
    
    if(isset($_POST['insert'])){
        $ad_id = $_POST['id'];
        $lname = $_POST['lname'];
        $pword = strtoupper($lname);
        $fname = $_POST['fname'];
        $mi = $_POST['mi'];
        $studnum = $_POST['studnum'];
        $email = $_POST['email'];
        $rfidnum = $_POST['rfidnum'];
        $total = str_replace(',','',$_POST['total']);
        $pay2 = $pay1 = $total * 0.35;
        $pay3 = $total - ($pay1 + $pay2);
        $string_pay1 = number_format($pay1,2,'.','');
        $string_pay2 = number_format($pay2,2,'.','');
        $string_pay3 = number_format($pay3,2,'.','');
        if(empty($rfidnum)){
            header("Location: index.php?id=".$ad_id."&msg=norfid");
            exit();
        }else{
            $sql = "SELECT * FROM tbl_students WHERE studnum ='".$studnum."'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result) === 0){
            $sql = "SELECT rfidnum FROM tbl_students WHERE rfidnum = '".$rfidnum."'";
            $result1 = mysqli_query($conn, $sql);
                if(mysqli_num_rows($result1) === 0){
                    $sql = "INSERT INTO tbl_students(studnum, pword, lname, fname, mi, email, rfidnum, pay1, pay2, pay3, balance) VALUES ('".$studnum."','".$pword."','".$lname."','".$fname."','".$mi."','".$email."','".$rfidnum."','".$string_pay1."','".$string_pay2."','".$string_pay3."','0.00')";
                    $result2 = mysqli_query($conn, $sql);
                    header("Location: index.php?id=".$ad_id."&msg=addsuccess");
                    exit();
                }else{
                    header("Location: index.php?id=".$ad_id."&msg=error");
                    exit();
                }

            }else{
                header("Location: index.php?id=".$ad_id."&msg=studnum1error");
                exit();
            }
        }  
    }else{
        header("Location: index.php?id=".$ad_id."&msg=error");
        exit();
    }
?>