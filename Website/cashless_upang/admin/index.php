<?php
    include "../db_con.php";
    $link_student=  "student/?id=".$_GET['id'];
    $link_teller =  "user/?id=".$_GET['id'];
    $link_tenants=  "tenants/?id=".$_GET['id'];
    $link_settings=  "settings/?id=".$_GET['id'];
    $sql = "SELECT * FROM tbl_accounts WHERE id = ".$_GET['id'];
    $result = mysqli_query($conn, $sql); 
    if(mysqli_num_rows($result) === 1){
        $row = mysqli_fetch_assoc($result);
            $admin_lname = $row['lname'];
            $admin_fname = $row['fname'];
            $admin_mi = $row['mi'];
    }
    $count_tennants = 0;
    $sql = "SELECT * FROM tbl_tennants";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) === 0){
    }else{
        $count_tennants = $count_tennants + 1;
    }
    $count_students = 0;
    $sql = "SELECT * FROM tbl_students";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) === 0){
    }else{
        $count_students = $count_students + 1;
    }

    $count_teller = 0;
    $sql = "SELECT * FROM tbl_accounts WHERE level='teller'";
    $result = mysqli_query($conn,$sql);
    if(mysqli_num_rows($result) === 0){
    }else{
        $count_teller = $count_teller + 1;
    }
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class="sidebar">
        <div class="logo-details">
        <i class="fa fa-coffee" aria-hidden="true"></i>
        <span class="logo_name">CUP UPANG</span>
        
        </div>
        <i class='bx bx-menu' id="btn"></i>
        <ul class="nav-links">
            <li>
                <a href="">
                    <i class='bx bxs-dashboard'></i>
                    <span class="link_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </li>
            <li>
                <a href="<?php echo $link_student?>">
                    <i class='bx bx-book-reader'></i>
                    <span class="link_name">Students</span>
                </a>
                <span class="tooltip">Students</span>
            </li>
            <li>
                <a href="<?php echo $link_tenants?>">
                    <i class='bx bx-basket'></i>
                    <span class="link_name">Tenants</span>
                </a>
                <span class="tooltip">Tenants</span>
            </li>
            <li>
                <a href="<?php echo $link_teller?>">
                    <i class='bx bxs-user'></i>
                    <span class="link_name">User Accounts</span>
                </a>
                <span class="tooltip">User Accounts</span>
            </li>
            <li>
                <a href="<?php echo $link_settings?>">
                    <i class='bx bx-cog'></i>
                    <span class="link_name">Settings</span>
                </a>
                <span class="tooltip">Settings</span>
            </li>
        </ul>
        <div class="profile_content">
            <div class="profile">
                <div class="profile_details">
                    <i class='bx bxs-user-circle' ></i>
                    <div class="name_job">
                        <div class="name"><?php echo $admin_lname.", ".$admin_fname." ".$admin_mi.".";?></div>
                        <div class="job">Administrator</div>
                </div>
                </div>
                <i class='bx bx-log-out' id=log_out></i>
            </div>
            
        </div>
        
    </div>
    <script>
        let btn = document.querySelector("#btn");
        let sidebar = document.querySelector(".sidebar");
        let logout = document.querySelector("#log_out");
        btn.onclick = function(){
            sidebar.classList.toggle("active");
        }
        logout.onclick = function(){
            var r = confirm("Are you sure you want to logout???");
            if(r==true){
                window.location.href = "../";
            }
        }
    </script>
    <div class="home-section">
        <div class="cardBox">
            <div class="card">
                <div>
                    <div class="numbers"><?php echo $count_teller;?></div>
                    <div class ="cardName">Registered Teller</div>
                </div>
                <div class="iconBox"><i class='bx bxs-receipt bx-rotate-180'></i></div>
            </div>
            <div class="card">
                <div>
                    <div class="numbers"><?php echo $count_tennants;?></div>
                    <div class ="cardName">Accredited Tenants</div>
                </div>
                <div class="iconBox"><i class='bx bxs-basket'></i></div>
            </div>
            <div class="card">
                <div>
                    <div class="numbers"><?php echo $count_students;?></div>
                    <div class ="cardName">Registered Students</div>
                </div>
                <div class="iconBox"><i class='bx bxs-book-reader'></i></div>
            </div>
        </div>
        <div class="details">
            <div class="loginActivity">
                <div class="loginHeader">
                    <h2>Login Activity</h2><br/>
                    <h3><?php echo "as of: " . date("M/d/Y");?></h3>
                </div>
                <div style="overflow: auto; max-height: 175px;"></div>
                <table>
                    <thead>
                        <tr>
                            <td>Username</td>
                            <td>Time</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sql = "SELECT * FROM tbl_logactivity WHERE date = CURRENT_DATE() ORDER BY time DESC";
                            $result = mysqli_query($conn, $sql); 
                            if($result){
                                while($row=mysqli_fetch_assoc($result)){
                                    ?>
                                    <tr>
                                        <td><?php echo $row['user'];?></th>
                                        <td id="date_time"><?php echo $row['time'];?></th>
                                    </tr>
                                    <?php
                                }
                            }else{
                            }
                            ?>
                    </tbody>
                </table>
            </div>
            <div class="emptySpace">
            </div>
        </div>
    </div>
</body>

</html>
<!-- <div class="home-content">
        <div class="overview-boxes">
            <div class="box">
                <div class="left-side">
                    <div class="box_topic">Tenants</div>
                    <div class="number"></div>
                    <div class="indicator">
                        <i class='bx bx-basket'></i>
                    </div>
                </div>
            </div>
        </div>
        </div> -->