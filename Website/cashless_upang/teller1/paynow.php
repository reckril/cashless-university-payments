<?php

    require_once '../DBOperations.php';
    require 'database.php';
    $response = array();
    $link = 'studpayment.php?id='.$_GET['id'];

        if(isset($_POST['studnum'])){
            $db = new DBOperation();

            if($db->userLogin($_POST['studnum'])){   
            $ticket = $db->getuserByusername($_POST['studnum']);
            if(floatval($ticket[$_POST["amount"]]) > floatval($ticket["balance"])){
                echo "<script>alert('Insufficient Balance')</script>";
                echo "<script>window.location.href='".$link."';</script>";
            }else{
                $amount = str_replace(",","",$ticket[$_POST["amount"]]);
                $amount2 = floatval($amount);
                $bal = str_replace(",","",$ticket["balance"]);
                $bal2 = floatval($bal);
                $total = number_format($bal2 - $amount2,2,".",",");
                $string_total = str_replace("float(","",strval($total));
                $pdo = Database::connect();
	    		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    		$sql = "UPDATE tbl_students set balance = ?, ".$_POST["amount"]." = ? WHERE studnum = ?";
	    		$q = $pdo->prepare($sql);
	    	    $q->execute(array($string_total,"0.00",$_POST['studnum']));
                $transcode = "tui";
                $paymethod = "ON-SITE";
                $transid= $_GET["id"];
                $pdo = Database::connect();
	            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	            $sql = "INSERT INTO tbl_transactions(transid, activity, studnum, rfid, paymethod, date, time, amount) values(?, ?, ?, ?, ?, CURRENT_DATE, CURRENT_TIME, ?)";
	            $q = $pdo->prepare($sql);
	            $q->execute(array($transid,$transcode,$ticket['studnum'],$ticket['rfidnum'],$paymethod,$amount2));
	            Database::disconnect();
                echo "<script>alert('Tuition Fee has been paid')</script>";
                echo "<script>window.location.href='".$link."';</script>";
            } 
                }
            else{
                echo "<script>alert('Transaction Failed.... Database Error')</script>";
                echo "<script>window.location.href='".$link."';</script>";
            }
        }else{
            echo "<script>alert('Transaction Failed.... Database Error')</script>";
            echo "<script>window.location.href='".$link."';</script>";
        }
    
?>