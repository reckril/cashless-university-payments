<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
    <style>
    .my_table tr {
        display:flex;
    }

    .my_table td {
        margin: 2px;
        height: 60px;
        display:flex;
        flex-grow:1;
        /* centering the button */
        align-items:center;
        justify-content:center;
    }

    tr{
        height: 50px;
    }
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
        }

        li {
            float: left;
        }

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

li a:visited{
    background-color: #111;
}
    .active {
        background-color: #4CAF50;
    }

body{
    background-image: linear-gradient(#0ba360,#3cba92);
}
    </style>
</head>
<body>
<ul>
        <li><a href="<?php echo '../teller/?id='.$_GET['id'];?>">Home</a></li>
        <li><a href="<?php echo 'stud.php?id='.$_GET['id'];?>">Student Registration</a></li>
        <li><a href="<?php echo 'studdeposit.php?id='.$_GET['id'];?>">Student Deposit</a></li>
        <li><a href="<?php echo 'studpayment.php?id='.$_GET['id'];?>">Student Payment</a></li>
        <li><a href="<?php echo 'translog.php?id='.$_GET['id'];?>">Transaction Logs</a></li>
        <li><a href="<?php echo 'accountsettings.php?id='.$_GET['id'];?>">Account Settings</a></li>
        <li style="float: right;"><a href="../" >Logout</a></li>
</ul>
</body>
</html>
<?php
    $username = "";
    $lname= "";
    $fname= "";
    $mi= "";
    $pword= "";
    include 'database.php';
    $pdo = Database::connect();
    $sql = 'SELECT * FROM tbl_accounts WHERE id="'.$_GET['id'].'"';
    foreach ($pdo->query($sql) as $row) {
    $username = $row['username'];
    $lname = $row['lname'];
    $fname= $row['fname'];
    $mi= $row['mi'];
    $pword= $row['pword'];
    $id = $row['id'];
}
Database::disconnect();

?>

<div style="border-style: solid; margin: auto; width: 30%; height:80%; background-color: white; margin-top: 20px;">
<div style="padding-top:5px; margin:auto;width: 80%;"><h1 style="text-align:center;">Account Settings</h1></div>
<div style="width:95%; height:70%; padding-top: 10px;">
    <form action="<?php echo 'editteller.php';?>" method="POST">
<input type="hidden" name="id" value='<?php echo $id;?>'>
<table style="width:80%;margin:auto;">
    <tr>
        <th >Last Name:</th>
        <td><input type="text" name="lname" required=required value='<?php echo $lname;?>'></td>
    </tr>
    <tr>
        <th>First Name:</th>
        <td><input type="text" name="fname" required=required value='<?php echo $fname;?>'></td>
    </tr>
    <tr>
        <th>Middle Initial:</th>
        <td><input type="text" name="mi" required=required value='<?php echo $mi;?>' maxlength="2"></td>
    </tr>
    <tr>
        <th>Username:</th>
        <td><input type="text" name="username" required=required value='<?php echo $username;?>'></td>
    </tr>
    <tr>
        <th>Password:</th>
        <td><input type="password" name="pword" required=required value='<?php echo $pword;?>'></td>
    </tr>
</table>
<div style="margin:auto; width:100%;"><input type="submit" name="submit" value="Continue" style="background-color: #4CAF50; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer;"></div>
</form>
</div>

</div>
