<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>

    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
        }

        li {
            float: left;
        }

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

li a:visited{
    background-color: #111;
}
    .active {
        background-color: #4CAF50;
    }

body{
        background-image: linear-gradient(#0ba360,#3cba92);
    }
    </style>
</head>
<body>
<ul>
        <li><a href="<?php echo '../teller/?id='.$_GET['id'];?>">Home</a></li>
        <li><a href="<?php echo 'stud.php?id='.$_GET['id'];?>">Student Registration</a></li>
        <li><a href="<?php echo 'studdeposit.php?id='.$_GET['id'];?>">Student Deposit</a></li>
        <li><a href="<?php echo 'studpayment.php?id='.$_GET['id'];?>">Student Payment</a></li>
        <li><a href="<?php echo 'translog.php?id='.$_GET['id'];?>">Transaction Logs</a></li>
        <li><a href="<?php echo 'accountsettings.php?id='.$_GET['id'];?>">Account Settings</a></li>
        <li style="float: right;"><a href="../" >Logout</a></li>
</ul>
<button class="btn">PRINT</button>
<div>
<table id="records3">
        <tr>
            <th>Student No.</th>
            <th>RFID No.</th>
            <th>Transaction</th>
            <th>Payment Method</th>
            <th>Amount</th>
        </tr>
    
    <?php
    include 'database.php';
        $pdo = Database::connect();
        $sql = 'SELECT * FROM tbl_transactions Where transid = "'.$_GET['id'].'" ORDER BY date AND time DESC';
        foreach ($pdo->query($sql) as $row){ 
        ?>
        <tr>
            <td><?php echo $row["studnum"];?></td>
            <td><?php echo $row["rfid"];?></td>
            <td><?php echo $row["activity"];?></td>
            <td><?php echo $row["paymethod"];?></td>
            <td><?php echo $row["amount"];?></td>
        </tr>
        <?php
        }
        Database::disconnect();
    ?>
</div>
<script src="js/jquery-3.4.1.js"></script>
             <script>
            $('.btn').click(function(){
            var printme= document.getElementById('records3');
            var wme = window.open("","","width=900,height=700");
            wme.document.write(printme.outerHTML);
            wme.focus();
            wme.print();
            wme.close();
            
        })
</script>

</body>
</html>