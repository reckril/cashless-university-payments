<?php
    require 'database.php';
    $id = null;
    if ( !empty($_GET['sid'])) {
        $id = $_REQUEST['sid'];
    }
	else{
		$id=$_GET['sid'];
	}
    $pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "SELECT * FROM tbl_students where rfidnum = ?";
	$q = $pdo->prepare($sql);
	$q->execute(array($id));
	$data = $q->fetch(PDO::FETCH_ASSOC);
	Database::disconnect();
	
	$msg = null;
	if($data == null){
		$msg = "This CUP Card is not registered !!!";
		$data['rfidnum']=$id;
		$data['studnum']="";
		$data['name']="";
	}else{
		$msg = null;
	}
	// if ($data['studnum']==null) {
	// 	$msg = "This CUP Card is not registered !!!";
	// 	$data['rfidnum']=$id;
	// 	$data['studnum']="";
	// 	$data['name']="";
	// } else {
	// 	$msg = null;
	// }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>
 
	<body>	
		<div>
			<form>
            RFID Number:
            <input type="text" name="rfidnum" required=required value="<?php echo $data['rfidnum'];?>"><br/>
            Student Number:
            <input type="text" name="studnum" required=required value="<?php echo $data['studnum'];?>"><br/>
            Name:
            <input type="text" name="name" required=required value="<?php echo ucfirst($data['lname']).', '.ucfirst($data['fname']).' '.ucfirst($data['mi']).'.';?>"><br/>
			Purpose:
            <select name="amount" required="required">
            <option value=""></option>
            <?php
                if($data["pay1"] == "0.00" || $data["pay1"] == null){

                }else{
                    echo "<option value='pay1'>1st Payment - Tuition Fee</option>";
                }
                if($data["pay2"] == "0.00" || $data["pay2"] == null){

                }else{
                    echo "<option value='pay2'>2nd Payment - Tuition Fee</option>";
                }
                if($data["pay3"] == "0.00" || $data["pay3"] == null){
                }else{
                    echo "<option value='pay3'>3nd Payment - Tuition Fee</option>";
                }
            ?>
            </select>
            </form>
		</div>
		<p style="color:red;"><?php echo $msg;?></p>
	</body>
</html>