<?php
	$Write="<?php $" . "UIDresult=''; " . "echo $" . "UIDresult;" . " ?>";
	file_put_contents('../UIDContainer.php',$Write);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.min.js"></script>
        <script>
			$(document).ready(function(){
				 $("#getUID").load("../UIDContainer.php");
				setInterval(function() {
					$("#getUID").load("../UIDContainer.php");
				}, 500);
			});
		</script>
        <style>
            ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
        }

        li {
            float: left;
        }

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

li a:visited{
    background-color: #111;
}
    .active {
        background-color: #4CAF50;
    }

body{
        background-image: linear-gradient(#0ba360,#3cba92);
    }
        </style>
</head>
<body>
<ul>
        <li><a href="<?php echo '../teller/?id='.$_GET['id'];?>">Home</a></li>
        <li><a href="<?php echo 'stud.php?id='.$_GET['id'];?>">Student Registration</a></li>
        <li><a href="<?php echo 'transactions.php?id='.$_GET['id'];?>">Transactions</a></li>
        <li><a href="<?php echo 'translog.php?id='.$_GET['id'];?>">Transaction Logs</a></li>
        <li><a href="<?php echo 'accountsettings.php?id='.$_GET['id'];?>">Account Settings</a></li>
        <li style="float: right;"><a href="../" >Logout</a></li>
     </ul>

     <table>
        <tr>
            <th>Student No.</th>
            <th>RFID No.</th>
            <th>Transaction</th>
            <th>Payment Method</th>
            <th>Amount</th>
        </tr>
    
    <?php
    include '../db_con.php';
        $sql = 'SELECT * FROM tbl_transactions Where transid = "'.$_GET['id'].'" ORDER BY date AND time DESC';
        $result = mysqli_query($conn, $sql);
        while($row=mysqli_fetch_assoc($result)){
        ?>
        <tr>
            <td><?php echo $row["studnum"];?></td>
            <td><?php echo $row["rfid"];?></td>
            <td><?php echo $row["activity"];?></td>
            <td><?php echo $row["paymethod"];?></td>
            <td><?php echo $row["amount"];?></td>
        </tr>
        <?php
        }
    ?>
    </table>
    -----------New Transaction-------------
    <form action="<?php echo "addtransaction.php?id=".$_GET['id'];?>" method="post">
    Transaction Type:
    <select name="s1" require=required>
    <option value="">---------</option>
    <option value="dep">Deposit Cash</option>
    <option value="tui">Tuition Fee</option>
    <!-- <option value="other">Others</option> -->
    </select><br/>
    ---------------------------------------<br/>
    <b id="blink">Please Tap CUP CARD inorder to auto-fill the text</b><br/>
    <p id="getUID" hidden></p>
    <div id="show_user_data">
    RFID Number:
    <input type="text" name="rfidnum" required=required><br/>
    Student Number:
    <input type="text" name="studnum" required=required><br/>
    Name:
    <input type="text" name="name" required=required><br/>
    </div>
    Amount:
    <input type="text" name="amount" required=required><br/>
    <input type="submit" value="SUBMIT">
    </form>
    <script>
			var myVar = setInterval(myTimer, 1000);
			var myVar1 = setInterval(myTimer1, 1000);
			var oldID="";
			clearInterval(myVar1);

			function myTimer() {
				var getID=document.getElementById("getUID").innerHTML;
				oldID=getID;
				if(getID!="") {
					myVar1 = setInterval(myTimer1, 500);
					showUser(getID);
					clearInterval(myVar);
				}
			}
			
			function myTimer1() {
				var getID=document.getElementById("getUID").innerHTML;
				if(oldID!=getID) {
					myVar = setInterval(myTimer, 500);
					clearInterval(myVar1);
				}
			}
			
			function showUser(str) {
				if (str == "") {
					document.getElementById("show_user_data").innerHTML = "";
					return;
				} else {
					if (window.XMLHttpRequest) {
						// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp = new XMLHttpRequest();
					} else {
						// code for IE6, IE5
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.onreadystatechange = function() {
						if (this.readyState == 4 && this.status == 200) {
							document.getElementById("show_user_data").innerHTML = this.responseText;
						}
					};
					xmlhttp.open("GET","read_tag.php?sid="+str,true);
					xmlhttp.send();
				}
			}
			
			var blink = document.getElementById('blink');
			setInterval(function() {
				blink.style.opacity = (blink.style.opacity == 0 ? 1 : 0);
			}, 750); 
		</script>
</body>
</html>