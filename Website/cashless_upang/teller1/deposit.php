<?php

    require_once '../DBOperations.php';
    require 'database.php';
    $response = array();
    $link = 'studpayment.php?id='.$_GET['id'];

        if(isset($_POST['studnum'])){
            $db = new DBOperation();

            if($db->userLogin($_POST['studnum'])){
                if($_POST["amount"] == "" || $_POST["amount"] == "0.00" || $_POST["amount"] == "0" || $_POST["amount"] == null){
                    echo "<script>alert('Transaction Denied.. Please Add Money')</script>";
                    echo "<script>window.location.href='".$link."';</script>";
                }else{
                    $ticket = $db->getuserByusername($_POST['studnum']);
                    $amount = str_replace(",","",$_POST['amount']);
                    $amount2 = floatval($amount);
                    $bal = str_replace(",","",$ticket["balance"]);
                    $bal2 = floatval($bal);
                    $total = number_format($amount2 + $bal2,2,".",",");
                    $string_total = str_replace("float(","",strval($total));
                    $pdo = Database::connect();
	    		    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    		    $sql = "UPDATE tbl_students set balance = ? WHERE studnum = ?";
	    		    $q = $pdo->prepare($sql);
	    	        $q->execute(array($string_total,$_POST['studnum']));
                    $transcode = "dep";
                    $paymethod = "ON-SITE";
                    $transid= $_POST["id"]-1;
                    $pdo = Database::connect();
	                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	                $sql = "INSERT INTO tbl_transactions(transid, activity, studnum, rfid, paymethod, date, time, amount) values(?, ?, ?, ?, ?, CURRENT_DATE, CURRENT_TIME, ?)";
	                $q = $pdo->prepare($sql);
	                $q->execute(array($transid,$transcode,$ticket['studnum'],$ticket['rfidnum'],$paymethod,$amount2));
	                Database::disconnect();
                    echo "<script>alert('Transaction is Accepted')</script>";
                    echo "<script>window.location.href='".$link."';</script>";
                    
                }
            }else{
                echo "<script>alert('Error with Server')</script>";
                echo "<script>window.location.href='".$link."';</script>";
            }
        }else{
            echo "<script>alert('Error with Server')</script>";
            echo "<script>window.location.href='".$link."';</script>";
        }
?>