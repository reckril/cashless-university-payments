<?php
    require_once '../DBOperations.php';
    require 'database.php';
    $det = true;
    $link = "transactions.php?id=".$_GET['id'];
    if(isset($_POST['studnum'])){
        $db = new DBOperation();
    if($_POST['s1'] == "dep"){
        

            if($db->userLogin($_POST['studnum'])){
                if($_POST["amount"] == "" || $_POST["amount"] == "0.00" || $_POST["amount"] == "0" || $_POST["amount"] == null){
                    $det=false;
                }else{
                    $ticket = $db->getuserByusername($_POST['studnum']);
                    $amount = str_replace(",","",$_POST['amount']);
                    $amount2 = floatval($amount);
                    $bal = str_replace(",","",$ticket["balance"]);
                    $bal2 = floatval($bal);
                    $total = number_format($amount2 + $bal2,2,".",",");
                    $string_total = str_replace("float(","",strval($total));
                    $pdo = Database::connect();
	    		    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    		    $sql = "UPDATE tbl_students set balance = ? WHERE studnum = ?";
	    		    $q = $pdo->prepare($sql);
	    	        $q->execute(array($string_total,$_POST['studnum']));
                    $transcode = "dep";
                    $paymethod = "ON-SITE";
                    $tellerid = $_GET['id'];
                    $pdo = Database::connect();
	                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	                $sql = "INSERT INTO tbl_transactions(activity, studnum, rfid, paymethod, transid, date, time, amount) values(?, ?, ?, ?, ?, CURRENT_DATE, CURRENT_TIME, ?)";
	                $q = $pdo->prepare($sql);
	                $q->execute(array($transcode,$ticket['studnum'],$ticket['rfidnum'],$paymethod,$tellerid,$amount2));
	                Database::disconnect();
                    $det=true;                
                }
            }else{
                $det=false;
            }
    }else{
        if($db->userLogin($_POST['studnum'])){   
            $ticket = $db->getuserByusername($_POST['studnum']);
            if(floatval($ticket[$_POST["s1"]]) > floatval($ticket["balance"])){
                $det = false;
            }else{
                $amount = str_replace(",","",$ticket[$_POST["amount"]]);
                $amount2 = floatval($amount);
                $bal = str_replace(",","",$ticket["balance"]);
                $bal2 = floatval($bal);
                $total = number_format($bal2 - $amount2,2,".",",");
                $string_total = str_replace("float(","",strval($total));
                $pdo = Database::connect();
	    		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    		$sql = "UPDATE tbl_students set balance = ?, ".$_POST["s1"]." = ? WHERE studnum = ?";
	    		$q = $pdo->prepare($sql);
	    	    $q->execute(array($string_total,"0.00",$_POST['studnum']));
                $transcode = "tum";
                $paymethod = "MOBILE";
                $pdo = Database::connect();
	            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	            $sql = "INSERT INTO tbl_transactions(activity, studnum, rfid, paymethod, date, time, amount) values(?, ?, ?, ?, CURRENT_DATE, CURRENT_TIME, ?)";
	            $q = $pdo->prepare($sql);
	            $q->execute(array($transcode,$ticket['studnum'],$ticket['rfidnum'],$paymethod,$amount2));
	            Database::disconnect();
                $det=true;
            } 
                }
            else{
                $det=false;
            }
        }else{
            $det=false;
        }
    }
    // if($det == true){
    //     $paymethod = "ON-SITE";
    //     $pdo = Database::connect();
	//     $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	//     $sql = "INSERT INTO tbl_transactions(activity, studnum, rfid, transid, paymethod, date, time, amount) values(?, ?, ?, ?, ?, CURRENT_DATE, CURRENT_TIME, ?)";
	//     $q = $pdo->prepare($sql);
	//     $q->execute(array($_POST['s1'],$studnum,$rfidnum,$_GET['id'],$paymethod,$amount));
	//     Database::disconnect();
    //     echo "<script>alert('Transaction is Accepted')</script>";
    //     echo "<script>window.location.href='".$link."';</script>";
    //  }
    // else{
    //     echo "<script>alert('No User Detection or Insufficient Funds')</script>";
    //     echo "<script>window.location.href='".$link."';</script>";
    // }
}
?>