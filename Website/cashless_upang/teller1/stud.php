<?php
	$Write="<?php $" . "UIDresult=''; " . "echo $" . "UIDresult;" . " ?>";
	file_put_contents('../UIDContainer.php',$Write);
?>
<!DOCTYPE html>
<html lang="en">
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta charset="utf-8">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.min.js"></script>
		<script>
			$(document).ready(function(){
				 $("#getUID").load("../UIDContainer.php");
				setInterval(function() {
					$("#getUID").load("../UIDContainer.php");
				}, 500);
			});
		</script>
        <style>
            body{
                background-image: linear-gradient(#0ba360,#3cba92);
            }
            ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
        }

        li {
            float: left;
        }

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

li a:visited{
    background-color: #111;
}
    .active {
        background-color: #4CAF50;
    }
        </style>
    </head>
    <body>
    <ul>
        <li><a href="<?php echo '../teller/?id='.$_GET['id'];?>">Home</a></li>
        <li><a href="<?php echo 'stud.php?id='.$_GET['id'];?>">Student Registration</a></li>
        <li><a href="<?php echo 'studdeposit.php?id='.$_GET['id'];?>">Student Deposit</a></li>
        <li><a href="<?php echo 'studpayment.php?id='.$_GET['id'];?>">Student Payment</a></li>
        <li><a href="<?php echo 'translog.php?id='.$_GET['id'];?>">Transaction Logs</a></li>
        <li><a href="<?php echo 'accountsettings.php?id='.$_GET['id'];?>">Account Settings</a></li>
        <li style="float: right;"><a href="../" >Logout</a></li>
</ul>

    <table>
        <tr>
            <th>RFID NO</th>
            <th>STUDENT NO.</th>
            <th>NAME</th>
            <th>1ST PAYMENT</th>
            <th>2ND PAYMENT</th>
            <th>3RD PAYMENT</th>
            <th>ACTION</th>
        </tr>
    
    <?php
    include 'database.php';
        $pdo = Database::connect();
        $sql = 'SELECT * FROM tbl_students ORDER BY studnum ASC';
        foreach ($pdo->query($sql) as $row){ 
        ?>
        <tr>
        <td><?php echo $row['rfidnum'];?></td>
        <td><?php echo $row['studnum'];?></td>
        <td><?php echo strtoupper($row['lname'].', '.$row['fname'].' '.$row['mi'].".");?></td>
        <td><?php echo $row['pay1'];?></td>
        <td><?php echo $row['pay2'];?></td>
        <td><?php echo $row['pay3'];?></td>
        <td><a href="<?php echo 'editstudent.php?id='.$_GET['id'].'&sid='.$row['id'];?>">Edit Balance</a>
        </td>
        </tr>
        <?php
        }
        Database::disconnect();
    ?>
    </table>
    ------Adding A Student------
    <form action="<?php echo 'addstudent.php';?>" method="POST">
        <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
        Last Name:<br/>
        <input type="text" name="lname" required=required><br/>
        First Name:<br/>
        <input type="text" name="fname" required=required><br/>
        Middle Initial:<br/>
        <input type="text" name="mi" required=required><br/>
        Student Number:<br/>
        <input type="text" name="studnum" required=required><br/>
        RFID Number:<br/>
        <textarea name="rfidnum" id="getUID" placeholder="Please Tag your Card / Key Chain to display ID" required=required></textarea><br/><br/><br/>
        Initial Tuition Fee Balance After Down Payment:<br/>
        <input type="text" name="balance" required=required><br/><br/>
        <input type="submit" name="submit" value="Confirm"><br/>
    </form>
    </div>
    </body>
</html>
</html>