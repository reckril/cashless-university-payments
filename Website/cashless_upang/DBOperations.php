<?php

    class DbOperation{
        private $con;

        function __construct(){

            require_once dirname(__FILE__).'/DBConnect.php';

            $db = new DbConnect();

            $this->con = $db->connect();
        }

        public function userLogin($username){
            //$password = md5($pass);
            $stmt = $this->con->prepare("SELECT id FROM tbl_students WHERE studnum = ?");
            //$stmt->bind_param("ss",$username,$password);
            $stmt->bind_param("s",$username);
            $stmt->execute();
            $stmt->store_result();
            return $stmt->num_rows > 0;
        }

        public function getUserByUsername($username){
            $stmt = $this->con->prepare("SELECT * FROM tbl_students WHERE studnum = ?");
            $stmt->bind_param("s",$username);
            $stmt->execute();
            return $stmt->get_result()->fetch_assoc();
        }

        public function showTransaction(){
            $stmt=$this->con->prepare("SELECT *, Month(date) AS 'Month', Day(date) AS 'Day', Year(date) AS 'Year', Hour(time)  AS 'Hour', Minute(time)  AS 'Minute' FROM tbl_transactions ORDER BY id DESC");
            $stmt->execute();
            $result=$stmt->get_result();
            return	$result;
        }

        public function showPrice(){
            $stmt=$this->con->prepare("SELECT * FROM tbl_otherpayment ORDER BY department AND course");
            $stmt->execute();
            $result=$stmt->get_result();
            return	$result;
        }
    }
    ?>