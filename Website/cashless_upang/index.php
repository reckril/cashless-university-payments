<html lang='en'>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cashless University Payments</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="js/bootstrap.min.js"></script>
</head>
<body>
    <form action="login.php" method="post">
        <h2>Cashless University Payments <br/>LOGIN</h2><br/>
        <?php if(isset($_GET['error'])){?>
            <p class="error"><?php if($_GET['error'] == "missing")echo "Enter Credentials!!! They Are Important" ; else if($_GET['error'] == "incorrect") echo "Incorrect Username and Password";?></p>
        <?php } ?>
        <label>Username:</label>
        <input type="text" name="user" placeholder="Enter Username"><br/>
        <label>Password:</label>
        <input type="password" name="pword" placeholder="Enter Password"><br/>
        <button type="submit">Login</button>
    </form>
    
</body>
</html>