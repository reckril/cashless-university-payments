<?php
    include "db_con.php";
    $no_account = 0;
    if(isset($_POST['user']) && isset($_POST['pword'])){
        $user = $_POST['user'];
        $pass = $_POST['pword'];

        if(empty($user)){
            header("Location: index.php?error=missing");
            exit();
        }else if(empty($pass)){
            header("Location: index.php?error=missing");
            exit();
        }else{
            $sql = "SELECT * FROM tbl_accounts WHERE username ='".$user."' AND pword = '".$pass."'";
            $result = mysqli_query($conn, $sql); 

            if(mysqli_num_rows($result) === 1){
                $row = mysqli_fetch_assoc($result);
                if($user === $row['username'] && $pass === $row['pword']){
                    if($row['level'] == "admin"){header("Location: admin/?id=".$row['id']); 
                        $sql = "INSERT INTO tbl_logactivity(user, date, time) VALUES('".$user."', CURRENT_DATE(), CURRENT_TIME())";
                        $result = mysqli_query($conn, $sql);
                        exit();}
                    else if($row['level'] == "teller"){ header("Location: teller/?id=".$row['id']); 
                        $sql = "INSERT INTO tbl_logactivity(user, date, time) VALUES('".$user."', CURRENT_DATE(), CURRENT_TIME())";
                        $result = mysqli_query($conn, $sql);
                        exit();}
                }else{
                    header("Location: index.php?error=incorrect");
                    exit();
                }
            }else{
                $sql = "SELECT * FROM tbl_students WHERE studnum ='".$user."' AND pword = '".$pass."'";
                $result = mysqli_query($conn, $sql); 

                if(mysqli_num_rows($result) === 1){
                    $row = mysqli_fetch_assoc($result);
                    if($user === $row['studnum'] && $pass === $row['pword']){
                        header("Location: student/?id=".$row['id']); 
                        $sql = "INSERT INTO tbl_logactivity(user, date, time) VALUES('".$user."', CURRENT_DATE(), CURRENT_TIME())";
                        $result = mysqli_query($conn, $sql);
                        exit();
                    }else{
                        header("Location: index.php?error=incorrect");
                        exit();
                    }
                }else{
                    $sql = "SELECT * FROM tbl_tennants WHERE user ='".$user."' AND pword = '".$pass."'";
                    $result = mysqli_query($conn, $sql); 

                    if(mysqli_num_rows($result) === 1){
                        $row = mysqli_fetch_assoc($result);
                        if($user === $row['user'] && $pass === $row['pword']){
                            header("Location: tenant/?id=".$row['id']); 
                            $sql = "INSERT INTO tbl_logactivity(user, date, time) VALUES('".$user."', CURRENT_DATE(), CURRENT_TIME())";
                            $result = mysqli_query($conn, $sql);
                            exit();
                        }else{
                            header("Location: index.php?error=incorrect");
                            exit();
                        }
                    }else{
                        header("Location: index.php?error=incorrect");
                        exit();
                    }
                }
            }
        }
    }else{
        header("Location: index.php");
        exit();
    }
?>