package com.example.cupupang;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class TransactorAdapter extends RecyclerView.Adapter<TransactorAdapter.TransactorViewHolder>{
    LayoutInflater inflater;
    List<Transactor> transactorList;

    public TransactorAdapter(Context context, List<Transactor> transactors){
        this.inflater = LayoutInflater.from(context);
        this.transactorList = transactors;
    }

    @Override
    public TransactorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_trans,parent,false);
        return new TransactorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TransactorAdapter.TransactorViewHolder holder, int position) {
        holder.dateTime.setText(transactorList.get(position).getDatesTime());
        holder.desc.setText(transactorList.get(position).getTransdescription());
        holder.amount.setText(transactorList.get(position).getAmount());
    }

    @Override
    public int getItemCount() {
        return transactorList.size();
    }

    class TransactorViewHolder extends RecyclerView.ViewHolder {
        TextView dateTime, desc, amount;
        public TransactorViewHolder(View itemView) {
            super(itemView);
            dateTime = itemView.findViewById(R.id.dateAndTime);
            desc = itemView.findViewById(R.id.transactionDescription);
            amount = itemView.findViewById(R.id.amount);
        }
    }
}
