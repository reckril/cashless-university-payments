package com.example.cupupang;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TransactionShow extends AppCompatActivity {
    RecyclerView recyclerView;
    List<Transactor> transactorList;
    private String URL_LINK = "http://192.168.1.20/cashless_upang/v1/transactions.php";
    TransactorAdapter transactorAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_show);
        recyclerView = findViewById(R.id.transactionRecycler);
        transactorList =  new ArrayList<>();
        
        extractTransactions();
    }

    private void extractTransactions() {
        Bundle bundle = getIntent().getExtras();
        RequestQueue queue = Volley.newRequestQueue(TransactionShow.this);
        JsonArrayRequest jsonArrayRequest =  new JsonArrayRequest(Request.Method.POST, URL_LINK, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject transactObject = response.getJSONObject(i);
                        if (transactObject.getString("studnum").equals(bundle.getString("studnum"))) {
                            Transactor transactor = new Transactor();
                            transactor.setDatesTime(transactObject.getString("datetime"));
                            transactor.setAmount(transactObject.getString("amount"));
                            transactor.setTransdescription(transactObject.getString("desc"));

                            transactorList.add(transactor);
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                transactorAdapter = new TransactorAdapter(getApplicationContext(),transactorList);
                recyclerView.setAdapter(transactorAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("tag", "onErrorResponse: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent launch = new Intent(TransactionShow.this, Hub.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }
}