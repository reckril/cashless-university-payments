package com.example.cupupang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AddOptions extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_options);
    }

    @Override
    public void onBackPressed() {
        Intent launch = new Intent(AddOptions.this, Hub.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }
    public void paymaya(View view){
        Intent launch = new Intent(AddOptions.this, Paymaya.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

    public void gcash(View view){
        Intent launch = new Intent(AddOptions.this, GCash.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

    public void seven(View view){
        Intent launch = new Intent(AddOptions.this, SevenEleven.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

    public void bpi(View view){
        Intent launch = new Intent(AddOptions.this, Bpi.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }
}