package com.example.cupupang;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vishnusivadas.advanced_httpurlconnection.PutData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OtherPayments extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    ArrayList<String> payList = new ArrayList<>();
    ArrayAdapter<String> payAdapter;
    Spinner spinPay;
    RequestQueue requestQueue;
    String Selected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_payments);
        requestQueue= Volley.newRequestQueue(this);
        spinPay = findViewById(R.id.spinOthers);
        String url = "http://192.168.1.20/cashless_upang/v1/showOthers.php";
        payList.add("Select ONE of the Events or Other Payments");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("payments");
                    for(int i = 0;i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String payName = jsonObject.optString("name");
                        payList.add(payName);
                        payAdapter = new ArrayAdapter<>(OtherPayments.this, android.R.layout.simple_spinner_item,payList);
                        payAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinPay.setAdapter(payAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonObjectRequest);
        spinPay.setOnItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent launch = new Intent(OtherPayments.this, Payments.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }
    public void back(View view){
        Intent launch = new Intent(OtherPayments.this, Payments.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        TextView textPrice;
        textPrice = (TextView) findViewById(R.id.textPrice);
        Selected = parent.getSelectedItem().toString();
        if(Selected.equals("Select ONE of the Events or Other Payments")){
            textPrice.setText("0.00");
        }else{
            String[] field = new String[1];
            String[] data = new String[1];
            field[0] = "event";
            data[0] = Selected;
            PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/checkEvent.php","POST",field,data);
            if(putData.startPut()) {
                if(putData.onComplete()){
                    String result = putData.getResult();
                    if(!result.equals("") && !result.equals("error")){
                        textPrice.setText(result);
                    }else{
                        textPrice.setText("0.00");
                    }
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void confirm(View view){
        Bundle bundle = getIntent().getExtras();
        if(Selected.equals("Select ONE of the Events or Other Payments") || Selected.equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(OtherPayments.this);
            builder.setTitle("Please Pick from One of the Events...");
            builder.setMessage("If There is no Events if there are no Events or Other Payments given...\nIf you are looking for Tuition Fee Payment there is a different button go back Please Select it...");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }else{
            String[] field = new String[2];
            String[] data = new String[2];

            field[0] = "event";
            field[1] = "studnum";
            data[0] = Selected;
            data[1] = bundle.getString("studnum");
            PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/payEvent.php","POST",field,data);
            if(putData.startPut()){
                if(putData.onComplete()){
                    String result = putData.getResult();
                    if(result.equals("Complete")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(OtherPayments.this);
                        builder.setTitle("Transaction is now complete!!");
                        builder.setMessage("Your Balance has been updated...");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent launch = new Intent(OtherPayments.this, Hub.class);
                                launch.putExtra("studnum",bundle.getString("studnum"));
                                startActivity(launch);
                                finish();
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                    }else if(result.equals("balance_short")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(OtherPayments.this);
                        builder.setTitle("Insufficient Balance");
                        builder.setMessage("Please Add More Money to this Account...");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                }
            }
        }
    }


}