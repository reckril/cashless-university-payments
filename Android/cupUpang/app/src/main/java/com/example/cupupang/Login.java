package com.example.cupupang;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.vishnusivadas.advanced_httpurlconnection.PutData;

import java.net.URL;
import java.util.Random;

public class Login extends AppCompatActivity {
    EditText textInputEditTextUsername, textInputEditTextPassword;
    Button buttonLog;
    TextView mess;
    String checkConn = "http://192.168.1.20/cashless_upang";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        textInputEditTextUsername = findViewById(R.id.txt_user);
        textInputEditTextPassword = findViewById(R.id.txt_pass);
        buttonLog = findViewById(R.id.btn_Login);
        mess = findViewById(R.id.textView2);
        buttonLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] field, data;
                field = new String[2];
                data = new String[2];
                field[0] = "user";
                field[1] = "pass";
                String user, pass;
                user = textInputEditTextUsername.getText().toString();
                pass = textInputEditTextPassword.getText().toString();
                data[0] = user;
                data[1] = pass;
                if (data[0].equals("") || data[1].equals("")) {
                    if(data[0].equals("") && !data[1].equals("")){
                        mess.setText("Please put Username and Password");
                    }else if(!data[0].equals("") && data[1].equals("")){

                    }
                    else{
                    mess.setText("Please put Username and Password");}
                } else {
                    PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/login.php", "POST", field, data);
                    if (putData.startPut()) {
                        if (putData.onComplete()) {
                            String result = putData.getResult();
                            //End ProgressBar (Set visibility to GONE)
                            if (result.equals("Login Successful")) {
                                Intent launch = new Intent(Login.this, Hub.class);
                                launch.putExtra("studnum", user);
                                startActivity(launch);
                                finish();
                            } else if (result.equals("Invalid User") || result.equals("Invalid UserPass")) {
                                mess.setText("Invalid Student ID and Password please try again");
                            } else if (result.equals("Missing Fields Detected")) {
                                mess.setText("Please Do Not Leave Blank Fields");
                            } else {
                                mess.setText("Connection Error");
                            }
                        }
                    }
                }
            }

        });


    }
    public void forget(View view){
        Intent launch = new Intent(Login.this, ForgetPass.class);
        startActivity(launch);
        finish();
    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit Application");
        builder.setMessage("Are you sure you want to leave");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Random rnd = new Random();
                String thanks;
                int x = (int) rnd.nextInt(10) ;
                if(x==0 || x==1) thanks = "App Has Been Closed Successfully";
                else if(x==2 || x==3) thanks = "Thanks For Stopping By!!";
                else if(x==4 || x==5) thanks = "Come Again Soon!!";
                else if(x==9) thanks = "See you later!!! Cheers!!!";
                else thanks = "Thank You Come Again";
                Toast.makeText(getApplicationContext(),thanks,
                        Toast.LENGTH_SHORT).show();
                finish();

            }
        });

        builder.show();
    }
}