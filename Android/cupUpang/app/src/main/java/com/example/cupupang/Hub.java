package com.example.cupupang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.vishnusivadas.advanced_httpurlconnection.PutData;

import java.util.ArrayList;
import java.util.Random;

public class Hub extends AppCompatActivity {
    String user;
    TextView txt_money;
    String bal;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hub);
        txt_money = findViewById(R.id.txt_money);
        Bundle bundle= getIntent().getExtras();
        user=bundle.getString("studnum");
        String[] field,data;
        field = new String[1];
        data = new String[1];
        field[0] = "user";
        data[0] = user;
        PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/balance.php", "POST", field, data);
        if (putData.startPut()) {
            if (putData.onComplete()) {
                String result = putData.getResult();
                //End ProgressBar (Set visibility to GONE)
                if (!result.equals("")) {
                    txt_money.setText(result);
                    bal = result;
                } else {
                }
            }

        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
//        String[] field,data;
//        field = new String[1];
//        data = new String[1];
//        field[0] = "user";
//        data[0] = user;
//        PutData putData = new PutData("http://192.168.1.20/cupUpang/v1/balance.php", "POST", field, data);
//        if (putData.startPut()) {
//            if (putData.onComplete()) {
//                String result = putData.getResult();
//                if (!result.equals("")) {
//                    txt_money.setText(result);
//                    bal = result;
//                } else {
//                }
//            }
//
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.account_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.Item2: changePass(); return true;
            case R.id.Item3: logout(); return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit Application");
        builder.setMessage("Are you sure you want to leave");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Random rnd = new Random();
                String thanks;
                int x = (int) rnd.nextInt(6) ;
                if(x==0 || x==1) thanks = "App Has Been Closed Successfully";
                else if(x==2 || x==3) thanks = "Thanks For Stopping By!!";
                else thanks = "Thank You Come Again";
                Toast.makeText(getApplicationContext(),thanks,
                        Toast.LENGTH_SHORT).show();
                finish();

            }
        });

        builder.show();
    }

    public void logout(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent launch = new Intent(Hub.this, Login.class);
                startActivity(launch);
                finish();
                Toast.makeText(getApplicationContext(),"Logout Successfully",
                        Toast.LENGTH_SHORT).show();
            }
        });

        builder.show();

    }

    public void paynow(View view){
        Intent launch = new Intent(Hub.this, Payments.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("balance",bal);
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }
    public void topup(View view){
        Intent launch = new Intent(Hub.this, AddOptions.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("balance",bal);
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }
    public void transact(View view){
        Intent launch = new Intent(Hub.this, TransactionShow.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("balance",bal);
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

    public void changePass(){
        Intent launch = new Intent(Hub.this, ChangePass.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

}