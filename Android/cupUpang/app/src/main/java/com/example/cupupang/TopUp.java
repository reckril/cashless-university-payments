package com.example.cupupang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.vishnusivadas.advanced_httpurlconnection.PutData;

import java.util.ArrayList;

public class TopUp extends AppCompatActivity {
//    int count = 0;
    TextView log,message;
    EditText textInputEditTextPhoneNumber, textInputEditTextAmount;
    ArrayList<String> payList = new ArrayList<>();
    ArrayAdapter<String> payAdapter;
    Spinner spinPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up);
        spinPay = findViewById(R.id.spinner3);
        log = findViewById(R.id.textView9);
        message = findViewById(R.id.txt_comp);
        payList.add("");
        payList.add("Paymaya");
        payList.add("GCash");
        payAdapter = new ArrayAdapter<>(TopUp.this, android.R.layout.simple_spinner_item, payList);
        payAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPay.setAdapter(payAdapter);


    }

    public void Continue(View view){
        textInputEditTextPhoneNumber = (EditText) findViewById(R.id.txt_phone);
        textInputEditTextAmount = (EditText) findViewById(R.id.txt_amount);

        Bundle bundle = getIntent().getExtras();
        String[] Field = new String[2];
        String[] Data = new String[2];
        Field[0] = "user";
        Field[1] = "amount";
        Data[0] = bundle.getString("studnum");
        Data[1] = textInputEditTextAmount.getText().toString();
        if(textInputEditTextAmount.equals("") || spinPay.equals("") || textInputEditTextPhoneNumber.equals("")){
           message.setText("Missing Fields");
        }else{
            PutData putData = new PutData("http://192.168.1.20/cupUpang/v1/deposit.php","POST",Field,Data);
          if (putData.startPut()) {
            if (putData.onComplete()) {
                String result = putData.getResult();
                if (result.equals("Deposit Complete")) {
                    Intent launch = new Intent(TopUp.this, Hub.class);
                    launch.putExtra("studnum", bundle.getString("studnum"));
                    startActivity(launch);
                    finish();
                    }
                }
            }
        }
//
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Bundle bundle = getIntent().getExtras();
        Intent launch = new Intent(TopUp.this, Hub.class);
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

    public void back(View view){
        Bundle bundle = getIntent().getExtras();
        Intent launch = new Intent(TopUp.this, Hub.class);
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

}