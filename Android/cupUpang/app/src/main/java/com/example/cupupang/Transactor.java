package com.example.cupupang;

public class Transactor {
    private String datesTime, transdescription, amount;

    public void setTransdescription(String transdescription) {
        this.transdescription = transdescription;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setDatesTime(String datesTime) {
        this.datesTime = datesTime;
    }

    public String getAmount() {
        return amount;
    }
    public String getDatesTime() {
        return datesTime;
    }
    public String getTransdescription() {
        return transdescription;
    }
}
