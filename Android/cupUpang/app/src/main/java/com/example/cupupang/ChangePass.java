package com.example.cupupang;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.vishnusivadas.advanced_httpurlconnection.PutData;

public class ChangePass extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

    }

    public void changePass(View view){
        EditText pass, rpass;
        Bundle bundle = getIntent().getExtras();
        pass = (EditText) findViewById(R.id.editTextTextPassChange2);
        rpass = (EditText) findViewById(R.id.editTextTextPassRChange2);
        String[] field = new String[3];
        String[] data = new String[3];
        field[0] = "pass";
        field[1] = "rpass";
        field[2] = "studnum";
        data[0] = pass.getText().toString();
        data[1] = rpass.getText().toString();
        data[2] = bundle.getString("studnum");
        if(data[0].equals("") || data[1].equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(ChangePass.this);
            builder.setTitle("Password Error");
            builder.setMessage("No Fields Found");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }else {
            PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/changePass.php", "POST", field, data);
            if (putData.startPut()) {
                if (putData.onComplete()) {
                    String result = putData.getResult();
                    if (result.equals("Complete")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ChangePass.this);
                        builder.setTitle("Complete");
                        builder.setMessage("Your Password has changed");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent launch = new Intent(ChangePass.this, Hub.class);
                                launch.putExtra("studnum",bundle.getString("studnum"));
                                startActivity(launch);
                                finish();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else if (result.equals("error")) {
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ChangePass.this);
                        builder.setTitle("Repeat Password Error");
                        builder.setMessage("Repeat Password should be the same as what you put in the new password");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }

                }
            }
        }
    }
}