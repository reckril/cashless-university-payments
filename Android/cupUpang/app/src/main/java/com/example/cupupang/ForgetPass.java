package com.example.cupupang;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.vishnusivadas.advanced_httpurlconnection.PutData;

public class ForgetPass extends AppCompatActivity {
    String final_email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);
//        rpass.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(txt_npass.getText().toString().equals(txt_rpass.getText().toString())) {
//                    if (txt_studnum.getText().toString().equals("") || txt_pass.getText().toString().equals("") || txt_npass.getText().toString().equals("") || txt_rpass.getText().toString().equals("")) {
//                        AlertDialog.Builder builder3 = new AlertDialog.Builder(getApplicationContext());
//                        builder3.setTitle("Error");
//                        builder3.setMessage("Enter all fields!!!");
//                        builder3.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
//                        builder3.show();
//                    }else {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                        builder.setTitle("Warning");
//                        builder.setMessage("Are you sure you want to Reset Your Password if so this cannot be undone.... and if you lost the password again you may want to contact us.....");
//                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        });
//                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                String[] field, data;
//                                field = new String[3];
//                                data = new String[3];
//                                field[0] = "user";
//                                field[1] = "opass";
//                                field[2] = "npass";
//                                String user, pass, npass;
//                                user = txt_studnum.getText().toString();
//                                pass = txt_pass.getText().toString();
//                                npass = txt_npass.getText().toString();
//                                data[0] = user;
//                                data[1] = pass;
//                                data[2] = npass;
//                                PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/forgetpass.php","POST",field,data);
//                                if (putData.startPut()) {
//                                    if (putData.onComplete()) {
//                                        String result = putData.getResult();
//                                        if(result.equals("Password Reset")){
//                                            AlertDialog.Builder builder2 = new AlertDialog.Builder(getApplicationContext());
//                                            builder2.setTitle("Password Reset Complete!");
//                                            builder2.setMessage("Your Password was Reset");
//                                            builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                                @Override
//                                                public void onClick(DialogInterface dialog, int which) {
//                                                    Intent launch = new Intent(ForgetPass.this, Login.class);
//                                                    startActivity(launch);
//                                                    finish();
//                                                }
//                                            });
//                                            builder2.show();
//                                        }
//                                        else if(result.equals("Wrong Old Password")){
//                                            AlertDialog.Builder builder2 = new AlertDialog.Builder(getApplicationContext());
//                                            builder2.setTitle("Wrong Old Password");
//                                            builder2.setMessage("It seems you don't know your Old Password.... Press the back key and choose another");
//                                            builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                                @Override
//                                                public void onClick(DialogInterface dialog, int which) {
//                                                }
//                                            });
//                                            builder2.show();
//                                        }
//                                    }
//                                }
//                            }
//                        });
//
//                        builder.show();
//                    }}
//                else{
//                    AlertDialog.Builder builder3 = new AlertDialog.Builder(getApplicationContext());
//                    builder3.setTitle("Error");
//                    builder3.setMessage("New Password and Repeated New Password is not the same");
//                    builder3.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
//                    builder3.show();
//                    }
//                }
//        });
//        rpass2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                String[] field, data;
//                field = new String[3];
//                data = new String[3];
//                field[0] = "user";
//                field[1] = "opass";
//                field[2] = "npass";
//                String user, pass, npass;
//                user = txt_studnum.getText().toString();
//                pass = txt_pass.getText().toString();
//                npass = txt_npass.getText().toString();
//                data[0] = user;
//                data[1] = pass;
//                data[2] = npass;
//                PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/forgetpass2.php","POST",field,data);
//                if (putData.startPut()) {
//                    if (putData.onComplete()) {
//                        String result = putData.getResult();
//                        if(result.equals("Email Found")){
//                            AlertDialog.Builder builder2 = new AlertDialog.Builder(getApplicationContext());
//                            builder2.setTitle("We have sent the reset password through your email");
//                            builder2.setMessage("It seems you don't know your Old Password.... Press the back key and choose another");
//                            builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    Intent launch = new Intent(ForgetPass.this, Login.class);
//                                    startActivity(launch);
//                                    finish();
//                                }
//                            });
//                            builder2.show();
//                        }
//                        else if(result.equals("Wrong Email and Student Number")){
//                            AlertDialog.Builder builder2 = new AlertDialog.Builder(getApplicationContext());
//                            builder2.setTitle("Wrong Old Password");
//                            builder2.setMessage("It seems you don't know your Student Number and Your Email.... Press the back key and choose Option 3, if not request a support ticket on the website");
//                            builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                }
//                            });
//                            builder2.show();
//                        }
//                    }
//                }
//            }
//        });
//
//        rpass3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String[] field, data;
//                field = new String[3];
//                data = new String[3];
//                field[0] = "email";
//                String email;
//                email = txt_email2.getText().toString();
//                data[0] = email;
//                PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/forgetpass3.php","POST",field,data);
//                if (putData.startPut()) {
//                    if (putData.onComplete()) {
//                        String result = putData.getResult();
//                        if(result.equals("Email Found")){
//                            AlertDialog.Builder builder2 = new AlertDialog.Builder(getApplicationContext());
//                            builder2.setTitle("We have sent the reset password through your email");
//                            builder2.setMessage("It seems you don't know your Old Password.... Press the back key and choose another");
//                            builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    Intent launch = new Intent(ForgetPass.this, Login.class);
//                                    startActivity(launch);
//                                    finish();
//                                }
//                            });
//                            builder2.show();
//                        }
//                        else if(result.equals("Wrong Email or Student Number")){
//                            AlertDialog.Builder builder2 = new AlertDialog.Builder(getApplicationContext());
//                            builder2.setTitle("Wrong Old Password");
//                            builder2.setMessage("Your Email.... If not request a support ticket on the website");
//                            builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                }
//                            });
//                            builder2.show();
//                        }
//                    }
//                }
//            }
//        });
    }

    @Override
    public void onBackPressed() {
        Button emailCheck, passReset;
        TextView emailText, passText, rpassText;
        EditText email, pass, rpass;
        emailCheck = (Button) findViewById(R.id.button4);
        emailText = (TextView) findViewById(R.id.emailText);
        email = (EditText) findViewById(R.id.editTextTextEmailCheck2);
        pass = (EditText) findViewById(R.id.editTextTextPassChange);
        passText = (TextView) findViewById(R.id.passText);
        passReset = (Button) findViewById(R.id.button3);
        rpass = (EditText) findViewById(R.id.editTextTextPassRChange);
        rpassText = (TextView) findViewById(R.id.rpassText);
        if(emailText.getVisibility() == View.INVISIBLE){
            email.setVisibility(View.VISIBLE);
            emailCheck.setVisibility(View.VISIBLE);
            emailText.setVisibility(View.VISIBLE);
            pass.setVisibility(View.INVISIBLE);
            passText.setVisibility(View.INVISIBLE);
            passReset.setVisibility(View.INVISIBLE);
            rpassText.setVisibility(View.INVISIBLE);
            rpass.setVisibility(View.INVISIBLE);
        }else{
            Intent launch = new Intent(ForgetPass.this, Login.class);
            startActivity(launch);
            finish();
        }
    }


    public void emailverify(View view){
        Button emailCheck, passReset;
        TextView emailText, passText, rpassText;
        EditText email, pass, rpass;
        emailCheck = (Button) findViewById(R.id.button4);
        emailText = (TextView) findViewById(R.id.emailText);
        email = (EditText) findViewById(R.id.editTextTextEmailCheck2);
        pass = (EditText) findViewById(R.id.editTextTextPassChange);
        passText = (TextView) findViewById(R.id.passText);
        passReset = (Button) findViewById(R.id.button3);
        rpass = (EditText) findViewById(R.id.editTextTextPassRChange);
        rpassText = (TextView) findViewById(R.id.rpassText);
        String[] field = new String[1];
        String[] data = new String[1];
        field[0] = "email";
        data[0] = email.getText().toString();
        if (data[0].equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPass.this);
            builder.setTitle("Email Error");
            builder.setMessage("No Fields Detected");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }else{
            PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/checkEmail.php","POST",field,data);
            if(putData.startPut()){
                if(putData.onComplete()){
                    String result = putData.getResult();
                    if(result.equals("Email Found")){
                        email.setVisibility(View.INVISIBLE);
                        emailCheck.setVisibility(View.INVISIBLE);
                        emailText.setVisibility(View.INVISIBLE);
                        pass.setVisibility(View.VISIBLE);
                        passText.setVisibility(View.VISIBLE);
                        passReset.setVisibility(View.VISIBLE);
                        rpassText.setVisibility(View.VISIBLE);
                        rpass.setVisibility(View.VISIBLE);
                        final_email = email.getText().toString();
                    }else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle("Email not found!");
                        builder.setMessage("It seems that this email isn't registered to anyone.\nPlease contact the office.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            }
        }
    }

    public void setPass(View view){
        EditText pass, rpass;
        pass = (EditText) findViewById(R.id.editTextTextPassChange);
        rpass = (EditText) findViewById(R.id.editTextTextPassRChange);
        String[] field = new String[3];
        String[] data = new String[3];
        field[0] = "pass";
        field[1] = "rpass";
        field[2] = "email";
        data[0] = pass.getText().toString();
        data[1] = rpass.getText().toString();
        data[2] = final_email;
        if(data[0].equals("") || data[1].equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPass.this);
                builder.setTitle("Password Error");
                builder.setMessage("No Fields Found");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
        }else{
            PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/passChange.php","POST",field,data);
                if(putData.startPut()){
                    if(putData.onComplete()){
                        String result = putData.getResult();
                        if(result.equals("Complete")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPass.this);
                            builder.setTitle("Complete");
                            builder.setMessage("Your Password has changed please make sure to never lose it again!");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent launch = new Intent(ForgetPass.this, Login.class);
                                    startActivity(launch);
                                    finish();
                                }
                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }else if(result.equals("error")){

                        }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                              builder.setTitle("Repeat Password Error");
                              builder.setMessage("Repeat Password should be the same as what you put in the new password");
                              builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                            });
                              AlertDialog alertDialog = builder.create();
                              alertDialog.show();
                        }

                    }
                }
        }
//        if(data[0].equals(data[1])){
//            if(data[0].equals("") || data[1].equals("")){
//
//            }else{
//
//            }
//
//        }else if(!data[0].equals(data[1])){
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Repeat Password Error");
//            builder.setMessage("Repeat Password should be the same as what you put in the new password");
//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                }
//            });
//        }else{
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Password Error");
//            builder.setMessage("No Fields Found");
//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                }
//            });}

    }
}