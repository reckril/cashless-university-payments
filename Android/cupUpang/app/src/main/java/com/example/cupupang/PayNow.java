package com.example.cupupang;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.vishnusivadas.advanced_httpurlconnection.PutData;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class PayNow extends AppCompatActivity {
    TextView viewMessage,pay1,pay2,pay3;
//    ArrayList<String> payList = new ArrayList<>();
//    ArrayAdapter<String> payAdapter;
//    Spinner spinPay;
    String paying1,paying2,paying3;
    float x,y,z;
//    Button btn_con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_now);
        pay1 = findViewById(R.id.txt_paym1);
        pay2 = findViewById(R.id.txt_paym2);
        pay3 = findViewById(R.id.txt_paym3);
//        spinPay = findViewById(R.id.spinPayFor);
        Bundle bundle = getIntent().getExtras();
        String[] Field  = new String[1], Data = new String[1];
        Field[0] = "user";
        Data[0] = bundle.getString("studnum");
        PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/payOne.php","POST",Field,Data);
        if (putData.startPut()) {
            if (putData.onComplete()) {
                String result = putData.getResult();
                pay1.setText(result);
                paying1 = result;
            }
        }
        putData = new PutData("http://192.168.1.20/cashless_upang/v1/payTwo.php","POST",Field,Data);
        if (putData.startPut()) {
            if (putData.onComplete()) {
                String result = putData.getResult();
                pay2.setText(result);
                paying2 = result;
            }
        }
        putData = new PutData("http://192.168.1.20/cashless_upang/v1/payTri.php","POST",Field,Data);
        if (putData.startPut()) {
            if (putData.onComplete()) {
                String result = putData.getResult();
                pay3.setText(result);
                paying3 = result;
            }
        }
//        if((paying1.equals("0.00") || paying1.equals("0")) && (paying2.equals("0.00") || paying2.equals("0")) && (paying1.equals("0.00") || paying1.equals("0"))){
//            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//            builder.setTitle("You Have Already Paid all your Tuition Fee");
//            builder.setMessage("Your Tuition Fee is fully paid!\nIf you have other fees please select the other option.");
//            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Bundle bundle = getIntent().getExtras();
//                    Intent launch = new Intent(PayNow.this, Hub.class);
//                    launch.putExtra("studnum",bundle.getString("studnum"));
//                    startActivity(launch);
//                    finish();
//                }
//            });
//            AlertDialog alertDialog = builder.create();
//            alertDialog.show();
//        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Bundle bundle = getIntent().getExtras();
        Intent launch = new Intent(PayNow.this, Payments.class);
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

    public void continue1(View view){
        Bundle bundle = getIntent().getExtras();
        EditText amount = (EditText) findViewById(R.id.editTextTextTuitionAmount);
        if(amount.getText().toString().equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(PayNow.this);
            builder.setTitle("Please put an amount");
            builder.setMessage("You need to put the amount");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }else{
            String[] field = new String[2];
            String[] data = new String[2];
            field[0] = "studnum";
            field[1] = "amount";
            data[0] = bundle.getString("studnum");
            data[1] = amount.getText().toString();
            PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/payTuition.php","POST",field,data);
            if(putData.startPut()){
                if(putData.onComplete()){
                    String result = putData.getResult();
                    if(result.equals("Complete")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(PayNow.this);
                        builder.setTitle("Tuition Fee paid");
                        builder.setMessage("Your Tuition Fee is paid!");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Bundle bundle = getIntent().getExtras();
                                Intent launch = new Intent(PayNow.this, Hub.class);
                                launch.putExtra("studnum",bundle.getString("studnum"));
                                startActivity(launch);
                                finish();
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }else if(result.equals("balance_short")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(PayNow.this);
                        builder.setTitle("Insufficient Balance");
                        builder.setMessage("Please add more money to continue");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }else if(result.equals("paid")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(PayNow.this);
                        builder.setTitle("Tuition Fee already paid");
                        builder.setMessage("Your Tuition Fee has no balance left");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                }
            }
        }

    }
    public void back1(View view){
        Bundle bundle = getIntent().getExtras();
        Intent launch = new Intent(PayNow.this, Payments.class);
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

}