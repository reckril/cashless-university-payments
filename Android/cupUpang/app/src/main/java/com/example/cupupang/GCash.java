package com.example.cupupang;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.vishnusivadas.advanced_httpurlconnection.PutData;

public class GCash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gcash);
    }

    @Override
    public void onBackPressed() {
        Intent launch = new Intent(GCash.this, AddOptions.class);
        Bundle bundle = getIntent().getExtras();
        launch.putExtra("studnum",bundle.getString("studnum"));
        startActivity(launch);
        finish();
    }

    public void deposit(View view){
        EditText editAmount, editNumber;
        float x;
        Bundle bundle = getIntent().getExtras();
        editNumber = (EditText) findViewById(R.id.editTextTextGCashNumber);
        editAmount = (EditText) findViewById(R.id.editTextTextGCashAmount);
        String[] field = new String[3];
        String[] data = new String[3];
        field[0] = "number";
        field[1] = "amount";
        field[2] = "studnum";
        data[0] = editNumber.getText().toString();
        data[1] = editAmount.getText().toString();
        x = Float.parseFloat(editAmount.getText().toString());
        data[2] = bundle.getString("studnum");
        if(data[0].equals("") || data[1].equals("")){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Put an amount");
            builder.setMessage("Please put an amount");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }else{
            if(x >= 50.00){
                PutData putData = new PutData("http://192.168.1.20/cashless_upang/v1/addGcash.php","POST",field,data);
                if(putData.startPut()){
                    if(putData.onComplete()){
                        String result = putData.getResult();
                        if(result.equals("Complete")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setTitle("Payment Successful");
                            builder.setMessage("Your Amount is updated..\n\nG-Cash will send the receipt and your current balance from them thank you!!!");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent launch = new Intent(GCash.this, Hub.class);
                                    launch.putExtra("studnum",bundle.getString("studnum"));
                                    startActivity(launch);
                                    finish();
                                }
                            });
                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                        }else{

                        }
                    }
                }
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Your Amount is lower");
                builder.setMessage("You need to have Higher than Php 50.00");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        }
    }
}