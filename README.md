# Cashless University Payments

## How to Download and Run this application

1. click on Code then Download the Source Code in ZIP file
2. Extract the ZIP file


**Required Applications:**
1. XAMPP
2. Android Studio
3. Arduino
4. Terminal (Mac) or Command Prompt (Windows)

Upon Extracting please follow these instructions

## For Website

```
**Front End: Website and API**
Note: You need this step followed as well for the Mobile Application to work

1. Click on the Project Folder and open Website folder 
2. Copy the contents and paste it on XAMPP's htdocs folder
3. On XAMPP App, Run to use APACHE and MYSQL (for Mac click Run All)

**Back End: MySQL PHPMyAdmin***
Note: Some cases where you cannot run there's an error to the programs please be sure to add this as its very crucial, If you already done Step 3 of the Front End skip Step 1 as its just the same.

1. On XAMPP, Run to use APACHE and MYSQL (for Mac click Run All)
2. Open any browser and type: localhost/phpmyadmin
3. On the right side click new and name the database as "db_cashless"
4. Click on Import then Import the data from the Project folder and Database Folder and click on the file named: "db_cashless.sql"


**Credentials:**
Admin:
user: admin
password: admin

Note: Remember to create an accounts for students and make sure the created hardware device is ready
```
## For Arduino

in Terminal:
1. type ipconfig and copy the local IP (ex: 192.168.x.x) on your desktop where XAMPP is being run

Hardware needed:
1. NodeMCU ESP8266 Wifi Board
2. RFID MFRC522
3. RFID Card
4. Wires Female to Female

Note: Make sure XAMPP is open and have APACHE and MySQL Running in the Background

CTTO: https://www.youtube.com/channel/UCk8rZ8lhAH4H-75tQ7Ljc1A

```
Critical Warning: Be sure to finish the "For Website" first before doing these steps

1. Open Arduino IDE and open the sketch file: sketch_may12a.ino
2. Do some following Changes:
 a. Line 37: Indicate Wifi Name in the Quotation mark
 b. Line 38: Indicate Wifi Password in the Quotation mark
 c. Line 100: Replace the IP of String "192.168.1.20" with your local IP address
3. As seen in Lines 7 to 19 follow the instructions given
4. Connect your Wifi Board to your computer and run the program with the target Board being the Wifi Board
```

## For Android

in Terminal:
1. type ipconfig and copy the local IP (ex: 192.168.x.x) on your desktop where XAMPP is being run

Note: Make sure XAMPP is open and have APACHE and MySQL Running in the Background

```
Critical Warning: Be sure to finish the "For Website" first before doing these steps

1. Open Android Studio and open the Project folder inside Android Folder
2. Do some following Changes:
  a. Login.java
    1. Line 27 : Replace the IP of String "192.168.1.20" with your local IP address
    2. Line 60 : Replace the IP of String "192.168.1.20" with your local IP address
  b. Paymaya.java and GCash.java
    1. Line 59 : Replace the IP of String "192.168.1.20" with your local IP address
  c. ForgetPass.java
    1. Line 254 : Replace the IP of String "192.168.1.20" with your local IP address
    2. Line 319 : Replace the IP of String "192.168.1.20" with your local IP address
  d. ChangePass.java
    1. Line 53 : Replace the IP of String "192.168.1.20" with your local IP address
  e. TransactionShow.java
    1. Line 28 : Replace the IP of String "192.168.1.20" with your local IP address
3. Connect your Android Phone be sure to put your Android Phone's Developer Settings and have USB Debugging ON
4. Find your phone in the upper right of Android Studio and Replace it with your device
5. Run the program the app will now be installed to your Android Phone
```

## Final Memo
If any of the programs are not running it could be that there are some depricated files be sure to contact me by my email so I can address the situation